# -*- coding: utf-8 -*-
{
    'name': "Cancel Remaining Sale Order Quantity",

    'summary': """Cancel Remaining Quantity from Sale Order Line""",

    'description': """
        This module adds a button in sale order line which will cancel undelevered quantity from line.
    """,


    'category': 'Sale',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/sale_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
    'installable': True,
    'application': True,
}
