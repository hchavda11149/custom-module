# -*- coding: utf-8 -*-

from odoo import models, fields, api

class sale_order_line_inherited(models.Model):
	_inherit = "sale.order.line"


	@api.multi
	def cancel_remaining_qty(self):
		for move in self.procurement_ids.mapped('move_ids').filtered(
				lambda r: r.state != 'done' and r.state != 'cancel'):

			# move_opration_link_obj = self.env['stock.move.operation.link'].search([('move_id','=',move.id)])
			opts = move.linked_move_operation_ids.mapped('operation_id')
			for operation in opts:
				operation.unlink()
			move.action_cancel()
			move.unlink()
		self.product_uom_qty = self.qty_delivered
		qty = self.qty_delivered

		for procurement_id in self.procurement_ids.filtered(lambda x: x.state != 'cancel' or x.state != 'done'):
			if qty >= procurement_id.product_qty:
				procurement_id.cancel()
				qty = qty - procurement_id.product_qty
			else:
				procurement_id.write({'product_qty': qty})
				break
		if self.product_uom_qty == 0:
			if self.env.context and not self.env.context.get('is_delete_line', False):
				self.state = 'cancel'
				self.unlink()
