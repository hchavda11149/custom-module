# -*- coding: utf-8 -*-
{
    'name': "Product Process Details",

    'summary': """
       Create Product Process""",


    'category': 'MRP',
    'version': '0.1',
    'depends': ['mrp','product'],
    'data': [
             'views/product_template_view.xml',
             'views/mrp_workcenter.xml',
             ],
    'installable': True,
    
    'application': True,
}
