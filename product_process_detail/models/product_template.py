from odoo import models, fields, api, _
from datetime import datetime

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    workcenter_ids = fields.Many2many('mrp.workcenter', string="Process Name")
    finished_routing_id = fields.Many2one('mrp.routing','Finished Routing Process')
    
    @api.multi
    def get_vals_workcenter(self, workcenter_ids):
        vals = {
            'name': str(self.name) + ' Process',
        }
        if workcenter_ids:
            operation_list = []
            for work_center in workcenter_ids:
                operation_list.append((0, 0, {
                    'name': work_center.name,
                    'workcenter_id': work_center and work_center.id or False,
                     'sequence': work_center.sequence,
                }))
            vals.update(operation_ids=operation_list)
        return vals

    @api.multi
    def update_workcenter(self, workcenter_ids):
        for record in workcenter_ids:
            work_center = self.finished_routing_id.operation_ids.filtered(lambda x: x.workcenter_id.id == record.id)
            if not work_center:
                vals = {
                    'workcenter_id': record.id,
                    'name': record.name,
                    'routing_id': self.finished_routing_id.id,
                    'sequence': work_center.sequence,
                }
                self.env['mrp.routing.workcenter'].create(vals)

        deleted_work_center = self.finished_routing_id.operation_ids.filtered(lambda x: x.workcenter_id.id not in workcenter_ids.ids)
        if deleted_work_center:
            deleted_work_center.unlink()

    @api.model
    def create(self, vals):
        res = super(ProductTemplate, self).create(vals)
        if res.sale_ok == True:
            if not res.finished_routing_id and res.workcenter_ids:
                work_vals = res.get_vals_workcenter(res.workcenter_ids)
                created_routing_id = self.env['mrp.routing'].create(work_vals)
                res.finished_routing_id = created_routing_id.id
        return res

    @api.multi
    def write(self, vals):
        res = super(ProductTemplate, self).write(vals)
        for data in self:
            if data.sale_ok == True and vals.get('workcenter_ids'):
                if not data.finished_routing_id:
                    work_vals = data.get_vals_workcenter(data.workcenter_ids)
                    created_routing_id = self.env['mrp.routing'].create(work_vals)
                    data.finished_routing_id = created_routing_id.id
                else:
                    work_vals = data.update_workcenter(data.workcenter_ids)
        return res


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.multi
    def get_vals_workcenter(self, workcenter_ids):
        vals = {
            'name': str(self.name) + ' Process',
        }
        if workcenter_ids:
            operation_list = []
            for work_center in workcenter_ids:
                operation_list.append((0, 0, {
                    'name': work_center.name,
                    'workcenter_id': work_center and work_center.id or False,
                    'sequence': work_center.sequence,
                }))
            vals.update(operation_ids=operation_list)
        return vals

    @api.model
    def create(self, vals):
        res = super(ProductProduct, self).create(vals)
        if res.sale_ok == True:
            if not res.finished_routing_id:
                work_vals = res.get_vals_workcenter(res.workcenter_ids)
                created_routing_id = self.env['mrp.routing'].create(work_vals)
                res.finished_routing_id = created_routing_id.id
        return res
