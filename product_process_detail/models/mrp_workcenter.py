from odoo import models, fields, api, _

class MRPWorkcenter(models.Model):
    
    _inherit = 'mrp.workcenter'
    
    
    @api.multi
    def update_sequence_into_routing_line(self,workcenter_ids,old_value,new_value):
        routing_line = self.env['mrp.routing.workcenter'].search([('workcenter_id','in',workcenter_ids.ids),('sequence','=',old_value)])
        if routing_line:
            routing_line.write({'sequence':new_value})
        
    @api.multi
    def write(self,vals):
        new_sequence = 0 
        if vals.get('sequence',0):
            for record in self:
                new_sequence = vals.get('sequence',0)
                self.update_sequence_into_routing_line(record, record.sequence, new_sequence)
            
        res = super(MRPWorkcenter,self).write(vals)
        return res
