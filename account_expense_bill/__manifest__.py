
{
    'name': 'Expense Bill',
    'version': '1.0',
    'category': 'Account',
    'sequence': 1,
    'summary': '',
    'description': """
        Add menu for expense bill. 
    """,

    'website': '',
    'depends': ['account'],
    'data': [
             'views/account_journal_view.xml',
             'views/expense_bill_menu.xml',
             ],   
    'events': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
