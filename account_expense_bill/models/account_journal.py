from odoo import models,fields,api,_

class AccountJournal(models.Model):
    
    _inherit = "account.journal"
    
    is_expense_journal = fields.Boolean('Expense Journal',copy=False,default=False)

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if context.get('is_expense_journal',False):
            args += [('is_expense_journal', '=', True)]
        return super(AccountJournal, self).search(args, offset, limit, order, count=count)
    
