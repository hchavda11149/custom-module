# -*- coding: utf-8 -*-

{
    'name': "Sale Stock Pending OR Reservation",

    'summary': """
        Set Configuration for Sale Stock Or DA 
        """,

    'description': """
        
        Default create DA When confirm sale order if not set configuration.
        If set reserve stock then reserve available stock to sale order and remain quantity show in pending.
        If set pending sale then all sale order display into pending list
    """,

    'category': 'sale',
    'version': '0.1',

    'depends': ['stock_available_unreserved','sale_stock','customer_po_date_field'],

    'data': [
             'security/ir.model.access.csv',
             'views/sale_stock_view.xml',
             'wizard/sale_config_setting.xml',
             'report/pending_order_report.xml',
             'wizard/create_dispatch_order_views.xml',
             'views/stock_picking.xml',
             'views/product_view.xml',
             'wizard/unreserve_product_stock.xml',
             'wizard/reserve_product_stock.xml',
             'wizard/reserve_when_confirm_so.xml',
    ],

    'application' : True,
    'installable' : True,

}
