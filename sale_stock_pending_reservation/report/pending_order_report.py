from odoo import api, fields, models, tools, _
from odoo.fields import Datetime as fieldsDatetime
from odoo.tools.sql import drop_view_if_exists
from odoo.exceptions import UserError, AccessError


class PendingOrderList(models.Model):
    _name = 'sale.pending.order.list'

    partner_id = fields.Many2one('res.partner', 'Customer Name')
    product_id = fields.Many2one('product.product', 'Product')
    customer_po_no = fields.Char("Customer PO No")
    customer_po_date = fields.Date('PO Date')
    qty = fields.Float('Ordered Qty.')
    delivered_qty = fields.Float('Dispatch Qty.')
    pending_qty = fields.Float('Pending Qty.')
    qty_available = fields.Float(related='product_id.qty_available_not_res', string='Qty. On Hand')
    sale_id = fields.Many2one('sale.order', 'Order No')
    sale_line_id = fields.Many2one('sale.order.line', 'Sale Line')
    qty_to_invoice = fields.Float('Invoice Quantity')
    qty_reserved = fields.Float('Reserved Qty.')

    _auto = False
    _order = 'id desc'

    @api.model
    def read_group(self,domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        res = super(PendingOrderList, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)
        if 'qty_available' in fields:
            for line in res:
                if '__domain' in line:
                    lines = self.search(line['__domain'])
                    total_available_qty = 0.0
                    for qty in lines:
                        total_available_qty += qty.qty_available
                    line['qty_available'] = total_available_qty
        return res



    @api.multi
    def open_wizard(self):
        if self.env.context and self.env.context.get('active_ids'):
            ready_val = []
            pending_val = []
            vals = {}
            pending_order = self.env['sale.pending.order.list'].browse(self.env.context.get('active_ids'))
            partner = pending_order.mapped('partner_id')
            if len(partner) > 1:
                raise UserError(_('Can not allow to select multiple customer'))

            vals.update({'partner_id': pending_order[0] and pending_order[0].partner_id and pending_order[
                0].partner_id.id or False})
            sale_id = pending_order.mapped('sale_id')
            if len(sale_id) == 1:
                vals.update({'order_id': pending_order[0] and pending_order[0].sale_id and pending_order[
                    0].sale_id.id or False})

            for line in pending_order:
                if line.pending_qty > 0 and line.pending_qty <= line.qty_available:
                    ready_dict = {
                        'sale_order_id': line.sale_id.id,
                        'sale_order_line_id': line.sale_line_id.id,
                        'product_id': line.product_id.id,
                        'available_pcs': line.product_id.qty_available_not_res,
                        'qty': line.pending_qty,
                        'total_pcs': line.pending_qty,
                    }

                    ready_val.append((0, 0, ready_dict))
                # elif line.pending_qty > 0:
                #     pending_val.append((0, 0, {
                #         'sale_order_id': line.sale_id.id,
                #         'sale_order_line_id': line.sale_line_id.id,
                #         'product_id': line.product_id.id,
                #         'std_pack': line.sale_line_id.std_pack,
                #         'total_carton': line.sale_line_id.total_carton,
                #         'weight': line.sale_line_id.weight,
                #         'kec_sheet_pouch': line.sale_line_id.kec_sheet_pouch,
                #         'kec_box': line.sale_line_id.kec_box,
                #         'kec_carton_packing': line.sale_line_id.kec_carton_packing,
                #         'available_pcs': line.product_id.qty_available_not_res,
                #         'available_carton': line.product_id.qty_available_not_res / line.sale_line_id.std_pack,
                #         'total_pcs': line.pending_qty,
                #     }))

            vals.update({
                'ready_orders': ready_val,
            })
            order_wizard_id = self.env['order.wizard'].create(vals)

            return {
                'name': 'Ready Pending Wizard',
                'view_mode': 'form',
                'view_type': 'form',
                'res_model': 'order.wizard',
                'res_id': order_wizard_id.id,
                'type': 'ir.actions.act_window',
                'target': 'new',
            }

    @api.model_cr
    def init(self):
        drop_view_if_exists(self._cr, 'sale_pending_order_list')
        self._cr.execute("""
                    CREATE or REPLACE VIEW sale_pending_order_list AS (
                    SELECT 
                        sol.id as id,
                        so.partner_id as partner_id,
                        sol.product_id as product_id,
                        (sol.product_uom_qty) as qty,
                        so.customer_po_no as customer_po_no,
                        so.customer_po_date as customer_po_date,
                        sol.qty_delivered as delivered_qty,
                        so.id as sale_id,sol.id as sale_line_id,
                        sol.qty_to_invoice as qty_to_invoice,
                        CASE WHEN (sol.product_uom_qty-sol.qty_delivered-sol.qty_reserved) > 0
                        THEN (sol.product_uom_qty-sol.qty_delivered-sol.qty_reserved) 
                        ElSE 0 END as pending_qty,
                        sol.qty_reserved as qty_reserved
                        FROM
                        sale_order so,
                        sale_order_line sol
                    WHERE
                        sol.order_id = so.id
                        and so.state in ('sale','done')
                        and (sol.product_uom_qty-sol.qty_delivered) > 0
                        
                    )""")

