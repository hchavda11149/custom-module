from odoo import models,fields,api,_

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    reserve_stock_move_ids = fields.One2many('stock.move','sale_order_reserve',domain=[('picking_id','=',False),('state','not in',('cancel','done'))])
    is_from_direct_da = fields.Boolean('From Direct DA',copy=False,default=False)
    is_from_pending_da = fields.Boolean('From Pending DA',copy=False,default=False)
    is_from_reserve_da = fields.Boolean('From Reserve DA',copy=False,default=False)
    is_when_confirm_so = fields.Boolean('From When Confirm SO',copy=False,default=False)
    
    @api.multi
    def cancel_reserve(self):            
        for line in self.order_line:
#            stock_moves = self.env['stock.move'].search([('picking_id','=',False),('sale_line_reserve','=',line.id),('sale_order_reserve','=',self.id),('state','not in',('cancel','done'))])
            cancel_qty = 0
            for move in line.reserve_stock_move_ids:
                cancel_qty+=move.product_uom_qty
                move.action_cancel()
            line.qty_reserved-=cancel_qty
            
        return
    
    @api.multi
    def action_cancel(self):
        res = super(SaleOrder,self).action_cancel()
        self.cancel_reserve()
        return res

    @api.multi
    def get_vals_reserved_stock_move(self,line,qty):
        reserve_location_id = self.env.ref('stock.stock_location_customers',
                                                           raise_if_not_found=False)
        stock_location_id = self.env.ref('stock.stock_location_stock', raise_if_not_found=False)
        vals = {
                'sale_order_reserve': line.order_id.id,
                'sale_line_reserve': line.id,
                'product_id': line.product_id.id,
                'product_uom_qty': qty,
                'product_uom': line.product_id.uom_id.id,
                'name': line.product_id.name,
                'location_id': stock_location_id.id,
                'location_dest_id': reserve_location_id.id,
                'origin':line.order_id.name
#                 'origin':self.name
            }
        return vals

    @api.multi
    def create_reserved_stock_move(self,line,qty):
        move_vals = self.get_vals_reserved_stock_move(line, qty)
        stock_move = self.env['stock.move'].create(move_vals)
        stock_move.action_confirm()
        stock_move.action_assign()
        line.qty_reserved += qty

    @api.multi
    def create_wizard_record(self):
        line_vals = []
        line_number = 0
        hide_lot = True
        for line in self.order_line:
            if line.product_id.qty_available_not_res > 0:
                if line.product_id.tracking == 'none': 
                    line_vals.append((0,0,{
                                           'product_id' : line.product_id.id,
                                           'order_qty': line.product_uom_qty,
                                           'sale_line_id' : line.id,
                                           'qty_available':line.product_id.qty_available_not_res,
                                           'line_number':line_number,
                                           }))
                else:
                    stock_quant_ids = self.env['stock.quant'].search([('location_id.usage','=', 'internal'),('product_id', '=', line.product_id.id),('reservation_id', '=', False)])
                    
                    stock_location = self.env.ref('stock.stock_location_stock')[0]
                    for lot in stock_quant_ids.mapped('lot_id'):
                        if stock_location:
                            quants = self.env['stock.quant'].search([('location_id.usage','=', 'internal'), ('lot_id', '=', lot.id),('product_id', '=', line.product_id.id), ('reservation_id', '=', False)])
                            qty = sum([x.qty for x in quants])
                            if qty > 0:
                                hide_lot = False
                                line_vals.append((0,0,{
                                           'product_id' : line.product_id.id,
                                           'order_qty': line.product_uom_qty,
                                           'sale_line_id' : line.id,
                                           'qty_available':qty,
                                           'lot_id':lot.id,
                                           'line_number':line_number,
                                           }))
                if line_number==0:
                    line_number = 1
                else:
                    line_number = 0
                
        vals = {
                'sale_order_id' : self.id,
                'reserve_confirm_so_line' : line_vals,
                }
        record = self.env['reserve.when.confirm.so'].create(vals)
        return record,hide_lot 
        
    @api.multi
    def action_confirm(self):
        res = super(SaleOrder,self).action_confirm()
        is_reserve_stock = self.env['ir.values'].get_default('sale.config.settings', 'is_reserve_stock')
        is_pending_sale = self.env['ir.values'].get_default('sale.config.settings', 'is_pending_sale')
        is_when_confirm_so = self.env['ir.values'].get_default('sale.config.settings', 'is_when_confirm_so')
        
        if not is_reserve_stock and not is_pending_sale and not is_when_confirm_so:
            self.write({'is_from_direct_da':True})
            return res
          
        for record in self:
            for picking in record.picking_ids:
                picking.action_cancel()
                
            if is_reserve_stock:
                record.is_from_reserve_da = True
                
                for line in record.order_line:
                    if line.product_id and line.product_id.qty_available_not_res > 0:
                        qty = 0
                        if line.product_uom_qty <= line.product_id.qty_available_not_res:
                            qty = line.product_uom_qty
                        else:
                            qty = line.product_id.qty_available_not_res
    
                        self.create_reserved_stock_move(line, qty)
                        
            elif is_when_confirm_so:
                record.is_when_confirm_so = True
                product_ids = record.order_line.mapped('product_id')
                exit_qty_product = product_ids.filtered(lambda x:x.qty_available_not_res > 0)
                if exit_qty_product:  
                    wizard_id,hide_lot = self.create_wizard_record()
                     
                    return {
                        'view_mode': 'form',
                        'res_id': wizard_id.id,
                        'res_model': 'reserve.when.confirm.so',
                        'view_type': 'form',
                        'type': 'ir.actions.act_window',
                        'target': 'new',
                        'context':{'hide_lot':hide_lot}
                    }

            else:
                record.is_from_pending_da = True
                        
        return res

class SaleOrderLine(models.Model):
    
    _inherit = 'sale.order.line'
    
    qty_reserved = fields.Float('Reserved',default=0,copy=False)
    reserve_stock_move_ids = fields.One2many('stock.move','sale_line_reserve',domain=[('picking_id','=',False),('state','not in',('cancel','done'))])

    @api.multi
    def _get_delivered_qty_from_move(self):
        self.ensure_one()
        qty = 0.0
        moves = self.env['stock.move'].search([('sale_order_line_id', '=', self.id), ('state', '=', 'done')])
        for move in moves:
            if move.location_dest_id.usage == "customer":
                if not move.origin_returned_move_id:
                    qty += move.product_uom._compute_quantity(move.product_uom_qty, self.product_uom)
            elif move.location_dest_id.usage != "customer" and move.to_refund_so:
                qty -= move.product_uom._compute_quantity(move.product_uom_qty, self.product_uom)
        return qty

    def _onchange_product_id_check_availability(self):
        return {}

class stockPickingInherited(models.Model):
    
    _inherit = "stock.picking"
    
    order_no = fields.Many2one('sale.order','Order No.')
    order_date = fields.Datetime('Order Date')
        
class StockMove(models.Model):
    
    _inherit = "stock.move"
    
    sale_order_reserve = fields.Many2one('sale.order', 'Reserve Sale Order')
    sale_line_reserve = fields.Many2one('sale.order.line', 'Reserve Sale Line')

    sale_id = fields.Many2one('sale.order', 'Sale Order(PO Number)')
    sale_order_line_id = fields.Many2one('sale.order.line', 'Sale Order Line')

    @api.multi
    def action_done(self):
        result = super(StockMove, self).action_done()

        for move in self:
            if move.sale_order_line_id:
                qty = move.sale_order_line_id._get_delivered_qty_from_move()
                move.sale_order_line_id.qty_delivered = qty
        return result
     


    


    
    