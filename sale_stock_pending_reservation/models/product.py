from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError

class Product(models.Model):
    _inherit = "product.product"

    pending_sale_qty = fields.Float(
        'Pending Sale Quantity', compute='_compute_sale_pending_quantities', 
        digits=dp.get_precision('Product Unit of Measure'))
        
    def _compute_sale_pending_quantities(self):
        
        for product in self:
            sale_line_ids = self.env['sale.order.line'].search([('product_id','=',product.id),('order_id.state','in',('sale','done')),('order_id.is_from_direct_da','=',False)])
            qty = 0
            for line in sale_line_ids:
                qty_delivered =line.qty_delivered+line.qty_reserved
                if line.product_uom_qty > qty_delivered:
                    qty+=line.product_uom_qty - qty_delivered
                    
            #qty = sum([(x.product_uom_qty - x.qty_delivered - x.qty_reserved) for x in sale_line_ids])
            product.pending_sale_qty = qty
            
class ProductTemplate(models.Model):
    _inherit = 'product.template'

    pending_sale_qty = fields.Float(
        'Pending Sale Quantity', compute='_compute_sale_pending_quantities', 
        digits=dp.get_precision('Product Unit of Measure'))

    def _compute_sale_pending_quantities(self):
        for product in self:
            product.pending_sale_qty = sum([p.pending_sale_qty for p in product.product_variant_ids])
