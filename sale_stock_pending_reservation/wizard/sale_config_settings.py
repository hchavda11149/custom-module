import logging

from odoo import api, fields, models

_logger = logging.getLogger(__name__)

class SaleConfiguration(models.TransientModel):
    
    _inherit = 'sale.config.settings'
    
    is_reserve_stock = fields.Boolean(string='Reserve Available Stock',default=False)
    is_pending_sale = fields.Boolean(string='Pending Sale List(All)',default=False)
    is_when_confirm_so = fields.Boolean('Reserve When Confirm SO',copy=False,default=False)
    according_stock_genearte_confirm_order = fields.Boolean('Allow to reserve more QTY than order QTY',default=False)
    according_stock_generate_dispatch_order = fields.Boolean('Allow to dispatch more QTY than order QTY',default=False)
    
    @api.multi
    def set_stock_generation_dispatch_order(self):
        return self.env['ir.values'].sudo().set_default(
            'sale.config.settings', 'according_stock_genearte_confirm_order', self.according_stock_genearte_confirm_order)
        
    @api.multi
    def set_according_stock_generate_dispatch_order(self):
        return self.env['ir.values'].sudo().set_default(
            'sale.config.settings', 'according_stock_generate_dispatch_order', self.according_stock_generate_dispatch_order)
    
    @api.multi
    def set_reserve_stock_defaults(self):
        return self.env['ir.values'].sudo().set_default(
            'sale.config.settings', 'is_reserve_stock', self.is_reserve_stock)

    @api.multi
    def set_pending_sale_defaults(self):
        return self.env['ir.values'].sudo().set_default(
            'sale.config.settings', 'is_pending_sale', self.is_pending_sale)

    @api.multi
    def set_confirm_sale_defaults(self):
        return self.env['ir.values'].sudo().set_default(
            'sale.config.settings', 'is_when_confirm_so', self.is_when_confirm_so)
    
    @api.onchange('is_reserve_stock')
    def onchange_reserve_stock(self):
        if self.is_pending_sale and self.is_reserve_stock:
            self.is_pending_sale = False
        if self.is_when_confirm_so and self.is_reserve_stock:
            self.is_when_confirm_so = False

    @api.onchange('is_pending_sale')
    def onchange_pending_stock(self):
        if self.is_reserve_stock and self.is_pending_sale:
            self.is_reserve_stock = False
        if self.is_when_confirm_so and self.is_pending_sale:
            self.is_when_confirm_so = False


    @api.onchange('is_when_confirm_so')
    def onchange_sale_confirm_stock(self):
        if self.is_reserve_stock and self.is_when_confirm_so:
            self.is_reserve_stock = False    
        if self.is_pending_sale and self.is_when_confirm_so:
            self.is_pending_sale = False    
                    