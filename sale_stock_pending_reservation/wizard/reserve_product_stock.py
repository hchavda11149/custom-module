from odoo import models, api, _, fields
from odoo.exceptions import UserError,ValidationError
from datetime import datetime, timedelta

class ReserveSaleOrder(models.TransientModel):
    _name = "reserve.order"
    _description = "reserve Sale Order"

    order_lines = fields.One2many('reserve.order.line', 'reserve_order_id')
              
    @api.model
    def _get_default_order_lines(self, pending_order):
        """
        Method used for get the default from the pending order and
        Set into delivery wizard line when create the delivery order from
        the action menu of the pending order list
        :param pending_order: pending order records
        :return: line values
        """
        line_values = []
        line_number = 0
        for order in pending_order:
            if order.pending_qty > 0 and order.product_id.qty_available_not_res > 0:
                if order.product_id.tracking == 'none':
                    line_values.append((0, 0, {'product_id': order.product_id and order.product_id.id,
                                           'sale_order_id': order.sale_id.id,
                                           'sale_line_id': order.sale_line_id.id,
                                           'qty_available':order.product_id and order.product_id.qty_available_not_res or 0,
                                           'pending_qty':order.pending_qty,
                                           'order_qty':order.qty,
                                           'line_number':line_number,
                                           }))
                else:
                    stock_quant_ids = self.env['stock.quant'].search([('location_id.usage','=', 'internal'),('product_id', '=', order.product_id.id),('reservation_id', '=', False)])
                    
                    stock_location = self.env.ref('stock.stock_location_stock')[0]
                    for lot in stock_quant_ids.mapped('lot_id'):
                        if stock_location:
                            quants = self.env['stock.quant'].search([('location_id.usage','=', 'internal'), ('lot_id', '=', lot.id),('product_id', '=', order.product_id.id), ('reservation_id', '=', False)])
                            qty = sum([x.qty for x in quants])
                            if qty > 0:
                                line_values.append((0, 0, {'product_id': order.product_id and order.product_id.id,
                                           'sale_order_id': order.sale_id.id,
                                           'sale_line_id': order.sale_line_id.id,
                                           'qty_available':qty,
                                           'pending_qty':order.pending_qty,
                                           'order_qty':order.qty,
                                           'lot_id' : lot.id,
                                           'line_number':line_number,
                                           }))
                if line_number==0:
                    line_number = 1
                else:
                    line_number = 0
                    
        return line_values

    @api.model
    def default_get(self, fields):
        res = super(ReserveSaleOrder, self).default_get(fields)
        if self.env.context and self.env.context.get('active_ids'):
            pending_order = self.env['sale.pending.order.list'].browse(self.env.context.get('active_ids'))
            if 'order_lines' in fields:
                res.update({'order_lines': self._get_default_order_lines(pending_order)})

        return res
      
    @api.multi
    def get_vals_reserved_stock_move(self,line,qty):
        reserve_location_id = self.env.ref('stock.stock_location_customers',
                                                           raise_if_not_found=False)
        stock_location_id = self.env.ref('stock.stock_location_stock', raise_if_not_found=False)
        vals = {
                'sale_order_reserve': line.sale_order_id.id,
                'sale_line_reserve': line.sale_line_id.id,
                'product_id': line.product_id.id,
                'product_uom_qty': qty,
                'product_uom': line.product_id.uom_id.id,
                'name': line.product_id.name,
                'location_id': stock_location_id.id,
                'location_dest_id': reserve_location_id.id,
                'origin':line.sale_order_id.name
            }
        if line.lot_id:
            vals.update({'restrict_lot_id':line.lot_id.id})
        return vals

    @api.multi
    def create_reserved_stock_move(self,line,qty):
        move_vals = self.get_vals_reserved_stock_move(line, qty)
        stock_move = self.env['stock.move'].create(move_vals)
        stock_move.action_confirm()
        stock_move.action_assign()
        line.sale_line_id.qty_reserved += qty

    @api.multi
    def create_reserve_order(self):
        according_stock_genearte_confirm_order = self.env['ir.values'].get_default('sale.config.settings', 'according_stock_genearte_confirm_order')
        for rec in self:
            for line in rec.order_lines:
                #total_reserve_qty = sum(x.to_reserve_qty for x in rec.order_lines.filtered(lambda l:l.product_id.id ==line.product_id.id))
                if line.to_reserve_qty > 0:
                    other_line = rec.order_lines.filtered(lambda x:x.to_reserve_qty > 0 and x.sale_line_id.id==line.sale_line_id.id)
                    total_reserve_qty = sum(x.to_reserve_qty for x in other_line)
                    if according_stock_genearte_confirm_order == False:
                        if total_reserve_qty > line.pending_qty:
                            raise UserError(_('Only %f quantity pending for product %s.But you reserve %f quantity')%(line.pending_qty,line.product_id.name,total_reserve_qty))
                    
                    lot_id = line.lot_id and line.lot_id.id or False
                    other_line = rec.order_lines.filtered(lambda l:l.to_reserve_qty > 0 and l.product_id.id ==line.product_id.id and l.lot_id.id == lot_id)
                    total_reserve_qty = sum(x.to_reserve_qty for x in other_line)
                    if total_reserve_qty > line.qty_available:
                        raise UserError(_('Only %f quantity on hand for product %s.But you reserve %f quantity')%(line.qty_available,line.product_id.name,total_reserve_qty))
    
                    to_reserve_qty = line.to_reserve_qty
                    self.create_reserved_stock_move(line, to_reserve_qty)
                
class ReserveSaleOrderLine(models.TransientModel):
    _name = "reserve.order.line"
    _description = "Reserve Sale Order"

    product_id = fields.Many2one('product.product', 'Product')
    reserve_order_id = fields.Many2one('reserve.order', 'Unreserve order')
    sale_order_id = fields.Many2one('sale.order', 'Sale Order')
    sale_line_id = fields.Many2one('sale.order.line', 'Sale Order Line')
    qty_available = fields.Float(string="Qty. On Hand")
    to_reserve_qty = fields.Float('To Reserve')
    pending_qty = fields.Float('Pending Qty')
    order_qty = fields.Float('Order Qty')
    lot_id = fields.Many2one('stock.production.lot','Lot')
    line_number = fields.Integer('Line Number',default=0)
    
    @api.onchange('to_reserve_qty')
    def oncahange_to_reserve(self):
        if self.to_reserve_qty < 0:
            self.to_reserve_qty = 0
            return {'warning': {'title': _("Warning"), 'message': 'Not allowed to set negative quantity'}}

        according_stock_genearte_confirm_order = self.env['ir.values'].get_default('sale.config.settings', 'according_stock_genearte_confirm_order')
        if according_stock_genearte_confirm_order == False:        
            if self.to_reserve_qty > self.pending_qty:
                self.to_reserve_qty = 0
                msg = 'You only allowed to Reserve '+str(self.pending_qty)+' quantity'
                return {'warning': {'title': _("Warning"), 'message': msg}}
        
        if self.to_reserve_qty > self.qty_available:
            self.to_reserve_qty = 0
            msg = 'You only allowed to Reserve '+str(self.qty_available)+' quantity'
            return {'warning': {'title': _("Warning"), 'message': msg}}