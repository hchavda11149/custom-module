import sys

from odoo import models, api, _, fields
from odoo.exceptions import UserError,ValidationError
from datetime import datetime, timedelta


class CreateDispatchOrder(models.TransientModel):
    _name = "create.dispatch.order"
    _description = "Create Dispatch Order"

    order_lines = fields.One2many('create.dispatch.order.line', 'dispatch_order_id')
    partner_id = fields.Many2one('res.partner', 'Partner')
    sale_order_id = fields.Many2one('sale.order', 'Sale Order')
    dispatch_date = fields.Date('Dispatch Date', default=fields.date.today())
    amount = fields.Float(compute='get_amount', string='Total Amount')
    
    @api.multi
    @api.depends('order_lines', 'order_lines.dispatch_qty', 'order_lines.unit_price')
    def get_amount(self):
        for record in self:
            record.amount = sum(x.amount for x in record.order_lines)

    @api.onchange('dispatch_date')
    def onchangedeliverydate(self):
        if self.dispatch_date:
            today_date = datetime.today().date()
            dispatch_date = datetime.strptime(self.dispatch_date, '%Y-%m-%d').date()
            if today_date > dispatch_date:
                    raise ValidationError(_("Back Date Entry is not Allowed"))
                
    @api.model
    def _get_default_order_lines(self, pending_order):
        """
        Method used for get the default from the pending order and
        Set into delivery wizard line when create the delivery order from
        the action menu of the pending order list
        :param pending_order: pending order records
        :return: line values
        """

        line_values = []
        for order in pending_order:
            line_values.append((0, 0, {'product_id': order.product_id and order.product_id.id,
                                       'pending_qty': order.pending_qty,
                                       'sale_order_id': order.sale_id.id,
                                       'sale_line_id': order.sale_line_id.id,
                                       'order_qty': order.sale_line_id.product_uom_qty,
                                       'unit_price': order.sale_line_id.price_unit,
                                       'reserved_qty':order.qty_reserved,
                                       'qty_available':order.qty_available,
                                       }))
        return line_values

    @api.model
    def default_get(self, fields):
        
        res = super(CreateDispatchOrder, self).default_get(fields)
        if self.env.context and self.env.context.get('active_ids'):

            pending_order = self.env['sale.pending.order.list'].browse(self.env.context.get('active_ids'))
            partner = pending_order.mapped('partner_id')
            sale_id = pending_order.mapped('sale_id')
            if len(partner) > 1:
                raise UserError(_('Can not allow to select multiple customer'))
            if len(sale_id) > 1:
                raise UserError(_('Not allowed to select multiple Orders'))
            if 'partner_id' in fields:
                res.update({'partner_id': pending_order[0] and pending_order[0].partner_id and pending_order[0].partner_id.id or False})
            if 'sale_order_id' in fields:
                res.update({
                    'sale_order_id': pending_order[0] and pending_order[0].sale_id and pending_order[0].sale_id.id or False,
                })
            if 'order_lines' in fields:
                res.update({'order_lines': self._get_default_order_lines(pending_order)})

        return res


    @api.multi
    def get_stock_picking_vals(self,move_lines,picking_type_id):

        return {
            'partner_id': self.partner_id.id,
            'move_lines': move_lines,
            'picking_type_id': picking_type_id.id,
            'order_no': self.sale_order_id.id or False,
            'order_date':self.sale_order_id.date_order,
            'origin': self.sale_order_id.name or False,
            'location_id': picking_type_id.default_location_src_id.id,
            'location_dest_id': self.partner_id.property_stock_customer.id,
        }

    @api.multi
    def get_stock_move_vals(self,line,picking_type_id,qty):
        
        return {
            'sale_order_id': line.sale_order_id.id,
            'sale_order_line_id': line.sale_line_id.id,
            'product_id': line.product_id.id,
            'product_uom_qty': qty,
            'product_uom': line.product_id.uom_id.id,
            'product_price_per_unit': line.unit_price,
            'name': line.product_id.name,
            'partner_id': line.sale_order_id.partner_shipping_id.id,
            'location_id': picking_type_id.default_location_src_id.id,
            'location_dest_id': self.partner_id.property_stock_customer.id,
            'scrapped': 0,

        }
      
    @api.multi
    def get_reserved_move_record(self,line,stock_move_ids,total_dispatch_qty):
        for reserve_move in line.sale_line_id.reserve_stock_move_ids:
            if reserve_move.product_uom_qty == total_dispatch_qty:
                stock_move_ids+=reserve_move
                total_dispatch_qty-=reserve_move.product_uom_qty
                line.sale_line_id.qty_reserved-=reserve_move.product_uom_qty
                reserve_move.sale_order_line_id = line.sale_line_id.id
                reserve_move.sale_order_id = line.sale_line_id.order_id.id
                break
            
            elif reserve_move.product_uom_qty < total_dispatch_qty:
                stock_move_ids+=reserve_move
                total_dispatch_qty-=reserve_move.product_uom_qty
                line.sale_line_id.qty_reserved-=reserve_move.product_uom_qty
                reserve_move.sale_order_line_id = line.sale_line_id.id
                reserve_move.sale_order_id = line.sale_line_id.order_id.id

            else:
                new_move = reserve_move.split(total_dispatch_qty)
                new_move = self.env['stock.move'].browse(new_move)
                
                if reserve_move.reserved_quant_ids:
                    Quant = self.env['stock.quant']
                    quants = Quant.quants_get_preferred_domain(new_move.product_uom_qty, new_move, ops=False, domain=[('id','in',reserve_move.reserved_quant_ids.ids)], preferred_domain_list=[])
                    self.env['stock.quant'].quants_reserve(quants,new_move)
                
                line.sale_line_id.qty_reserved-=new_move.product_uom_qty
                new_move.sale_order_line_id = line.sale_line_id.id
                new_move.sale_order_id = line.sale_line_id.order_id.id                
                stock_move_ids+=new_move
                total_dispatch_qty=0 
                break
                
                
        return stock_move_ids,total_dispatch_qty
        
    @api.multi
    def create_delivery_order(self):
        
        stock_picking_obj = self.env['stock.picking']
        picking_type_id = self.env.ref('stock.picking_type_out', raise_if_not_found=False)

        move_lines = []
        stock_move_ids = self.env['stock.move']
        according_stock_generate_dispatch_order = self.env['ir.values'].get_default('sale.config.settings', 'according_stock_generate_dispatch_order')
        
        for rec in self:
            for line in rec.order_lines:
                if line.dispatch_qty == 0:
                    raise UserError(_("You can't Dispatch 0 Quantity"))
                
                if according_stock_generate_dispatch_order ==False:
                    total_pending_qty = line.pending_qty+line.reserved_qty
                    if line.dispatch_qty > total_pending_qty:
                        raise UserError(_('For Product %s You can not dispatch more than %s quantity'%(line.product_id.name, total_pending_qty)))
                
                if according_stock_generate_dispatch_order == False:
                    total_exist = line.reserved_qty + line.qty_available
                    if line.dispatch_qty > total_exist:
                        raise UserError(_('For Product %s You have %s quantity in stock'%(line.product_id.name, total_exist)))
                
                if according_stock_generate_dispatch_order == True:
                    total_exist = line.reserved_qty + line.qty_available
                    if line.dispatch_qty > total_exist:
                        raise UserError(_('For Product %s You have %s quantity in stock')%(line.product_id.name,line.qty_available))
                        

                total_dispatch_qty = line.dispatch_qty
                stock_move_ids,total_dispatch_qty = self.get_reserved_move_record(line,stock_move_ids,total_dispatch_qty)
                
                if total_dispatch_qty > 0:
                    move_line_vals =self.get_stock_move_vals(line, picking_type_id,total_dispatch_qty)
                    move_lines.append((0, 0,move_line_vals))
                
            if len(move_lines) > 0 or stock_move_ids:
                picking_vals = self.get_stock_picking_vals(move_lines,picking_type_id)
                created_stock_picking_id = stock_picking_obj.create(picking_vals)
                if stock_move_ids:
                    stock_move_ids.write({'picking_id':created_stock_picking_id.id})
                
                created_stock_picking_id.action_assign()
                created_stock_picking_id.action_confirm()
                
                return {
                    'name': 'Make DA',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'res_model': 'stock.picking',
                    'res_id': created_stock_picking_id.id,
                    'type': 'ir.actions.act_window',
                }
            else:
                return {'value': {}, 'warning': {'title': 'Warning', 'message': 'Can Not Create DA with 0 Products'}}


class CreateDeliveryOrderLine(models.TransientModel):

    _name = "create.dispatch.order.line"

    product_id = fields.Many2one('product.product', 'Product')
    dispatch_order_id = fields.Many2one('create.dispatch.order', 'Dispatch order')
    dispatch_qty = fields.Float('To Dispatch Quantity')
    order_qty = fields.Float('Order Quantity')
    pending_qty = fields.Float('Pending')
    unit_price = fields.Float('Unit Price', readonly=True)
    sale_order_id = fields.Many2one('sale.order', 'Sale Order')
    sale_line_id = fields.Many2one('sale.order.line', 'Sale Order Line')
    amount = fields.Float(compute='get_amount', string='Amount')
    qty_available = fields.Float(string='Qty. On Hand')
    reserved_qty = fields.Float('Reserved')
    
    @api.multi
    @api.depends('dispatch_qty', 'unit_price')
    def get_amount(self):
        for line in self:
            line.amount = line.unit_price * line.dispatch_qty






