from odoo import models,fields,api,_
from odoo.exceptions import UserError,ValidationError
import sys

class ReserveWhenConfirmSO(models.Model):
    
    _name = 'reserve.when.confirm.so'
    
    sale_order_id = fields.Many2one('sale.order','Sale Order')
    reserve_confirm_so_line = fields.One2many('reserve.when.confirm.so.line','reserve_confirm_id','Lines')
    
    
    def create_reserve_order(self):
        according_stock_genearte_confirm_order = self.env['ir.values'].get_default('sale.config.settings', 'according_stock_genearte_confirm_order')
       
        for line in self.reserve_confirm_so_line.filtered(lambda x:x.to_reserve_qty > 0):
            other_line = self.reserve_confirm_so_line.filtered(lambda x:x.to_reserve_qty > 0 and x.sale_line_id.id==line.sale_line_id.id)
            
            if according_stock_genearte_confirm_order == False:
                total_qty = sum(x.to_reserve_qty for x in other_line)
                if total_qty > line.order_qty:
                    raise UserError(_('You only reserve %f quantity for product %s.But you reserve %f quantity')%(line.order_qty,line.product_id.name,total_qty))
            
            lot_id = line.lot_id and line.lot_id.id or False
            other_line = self.reserve_confirm_so_line.filtered(lambda l:l.to_reserve_qty > 0 and l.product_id.id ==line.product_id.id and l.lot_id.id == lot_id)
           
            if according_stock_genearte_confirm_order == False:
                total_reserve_qty = sum(x.to_reserve_qty for x in other_line)
                if total_reserve_qty > line.qty_available:
                    raise UserError(_('Only %f quantity on hand for product %s.But you reserve %f quantity')%(line.qty_available,line.product_id.name,total_reserve_qty))
            
            stock_vals = self.sale_order_id.get_vals_reserved_stock_move(line.sale_line_id,line.to_reserve_qty)
            if line.lot_id:
                stock_vals.update({'restrict_lot_id':line.lot_id.id})
            stock_move = self.env['stock.move'].create(stock_vals)
            stock_move.action_confirm()
            stock_move.action_assign()
            line.sale_line_id.qty_reserved += line.to_reserve_qty
            
        return True
    
class ReserveWhenConfirmSOLine(models.Model):
    
    _name = 'reserve.when.confirm.so.line'
    
    reserve_confirm_id = fields.Many2one('reserve.when.confirm.so','Reserve Confirm')
    product_id = fields.Many2one('product.product','Product')
    tracking = fields.Selection(related='product_id.tracking',string='Tracking')
    qty_available = fields.Float(string='Stock Qty.')
    to_reserve_qty = fields.Float('To Reserve Qty')
    order_qty = fields.Float('Order Qty.')
    sale_line_id = fields.Many2one('sale.order.line','Order Line')
    lot_id = fields.Many2one('stock.production.lot','Lot')
    line_number = fields.Integer('Line Number',default=0)
    
    @api.onchange('to_reserve_qty')
    def oncahange_to_reserve(self):
        if self.to_reserve_qty < 0:
            self.to_reserve_qty = 0
            return {'warning': {'title': _("Warning"), 'message': 'Not allowed to set negative quantity'}}
        
        
        according_stock_genearte_confirm_order = self.env['ir.values'].get_default('sale.config.settings', 'according_stock_genearte_confirm_order')
       
        if according_stock_genearte_confirm_order == False:
            if self.to_reserve_qty > self.order_qty:
                self.to_reserve_qty = 0
                msg = 'You only allowed to Reserve '+str(self.order_qty)+' quantity'
                raise UserError(_(msg))
#                 return {'warning': {'title': _("Warning"), 'message': msg}}
            
        if according_stock_genearte_confirm_order == False:
            if self.to_reserve_qty > self.qty_available:
                self.to_reserve_qty = 0
                msg = 'You only allowed to Reserve '+str(self.qty_available)+' quantity'
                raise UserError(_(msg))
#                 return {'warning': {'title': _("Warning"), 'message': msg}}   
        
        if according_stock_genearte_confirm_order == True:
                if self.qty_available < self.to_reserve_qty:
                    self.to_reserve_qty = 0
                    msg = 'You only allowed to Reserve '+str(self.qty_available)+' quantity'
                    raise UserError(_(msg))
#                     return {'warning': {'title': _("Warning"), 'message': msg}}         
#                     return UserError(_('You only reserve %f quantity for product %s')%(self.qty_available,self.product_id.name))
            
            
#         if self.to_reserve_qty > self.qty_available:
#             self.to_reserve_qty = 0
#             msg = 'You only allowed to Reserve '+str(self.qty_available)+' quantity'
#             return {'warning': {'title': _("Warning"), 'message': msg}}    
#         
        
        
        