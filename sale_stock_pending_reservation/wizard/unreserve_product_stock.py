from odoo import models, api, _, fields
from odoo.exceptions import UserError,ValidationError
from datetime import datetime, timedelta

class UnreserveSaleOrder(models.TransientModel):
    _name = "unreserve.order"
    _description = "Unreserve Sale Order"

    order_lines = fields.One2many('unreserve.order.line', 'unreserve_order_id')
                  
    @api.model
    def _get_default_order_lines(self, pending_order):
        """
        Method used for get the default from the pending order and
        Set into delivery wizard line when create the delivery order from
        the action menu of the pending order list
        :param pending_order: pending order records
        :return: line values
        """

        line_values = []
        line_number = 0
        for order in pending_order:
            if order.qty_reserved > 0:
                if order.product_id.tracking == 'none':  
                    line_values.append((0, 0, {'product_id': order.product_id and order.product_id.id,
                                           'sale_order_id': order.sale_id.id,
                                           'sale_line_id': order.sale_line_id.id,
                                           'reserved_qty':order.qty_reserved,
                                           'order_qty':order.qty,
                                           'line_number':line_number,
                                           }))
                else:
                    quants = self.env['stock.quant']
                    for move in order.sale_line_id.reserve_stock_move_ids:
                        quants += quants.search([('reservation_id','=',move.id)])
                         
                    for lot in quants.mapped('lot_id'):
                        lot_quants = quants.filtered(lambda x:x.lot_id.id==lot.id) 
                        
                        qty = sum([x.qty for x in lot_quants])
                        if qty > 0:
                            
                            line_values.append((0, 0, {'product_id': order.product_id and order.product_id.id,
                                           'sale_order_id': order.sale_id.id,
                                           'sale_line_id': order.sale_line_id.id,
                                           'reserved_qty':qty,
                                           'order_qty':order.qty,
                                           'lot_id' : lot.id,
                                           'line_number':line_number,
                                           }))
                if line_number==0:
                    line_number = 1
                else:
                    line_number = 0
        return line_values

    @api.model
    def default_get(self, fields):
        res = super(UnreserveSaleOrder, self).default_get(fields)
        if self.env.context and self.env.context.get('active_ids'):
            pending_order = self.env['sale.pending.order.list'].browse(self.env.context.get('active_ids'))
            order_lines = self._get_default_order_lines(pending_order)
            if 'order_lines' in fields:
                res.update({'order_lines': order_lines})
        return res
      
    @api.multi
    def get_reserved_move_record(self,line,stock_move_ids,total_dispatch_qty):
        
        if line.product_id.tracking == 'none':  
            for reserve_move in line.sale_line_id.reserve_stock_move_ids:
                if reserve_move.product_uom_qty == total_dispatch_qty:
                    stock_move_ids+=reserve_move
                    total_dispatch_qty-=reserve_move.product_uom_qty
                    line.sale_line_id.qty_reserved-=reserve_move.product_uom_qty
                    reserve_move.sale_order_line_id = line.sale_line_id.id
                    reserve_move.sale_order_id = line.sale_line_id.order_id.id
                    break
                
                elif reserve_move.product_uom_qty < total_dispatch_qty:
                    stock_move_ids+=reserve_move
                    total_dispatch_qty-=reserve_move.product_uom_qty
                    line.sale_line_id.qty_reserved-=reserve_move.product_uom_qty
                    reserve_move.sale_order_line_id = line.sale_line_id.id
                    reserve_move.sale_order_id = line.sale_line_id.order_id.id
    
                else:
                    new_move = reserve_move.split(total_dispatch_qty)
                    new_move = self.env['stock.move'].browse(new_move)
                    
                    if reserve_move.reserved_quant_ids:
                        Quant = self.env['stock.quant']
                        quants = Quant.quants_get_preferred_domain(new_move.product_uom_qty, new_move, ops=False, domain=[('id','in',reserve_move.reserved_quant_ids.ids)], preferred_domain_list=[])
                        self.env['stock.quant'].quants_reserve(quants,new_move)
                    
                    line.sale_line_id.qty_reserved-=new_move.product_uom_qty
                    new_move.sale_order_line_id = line.sale_line_id.id
                    new_move.sale_order_id = line.sale_line_id.order_id.id                
                    stock_move_ids+=new_move
                    total_dispatch_qty=0 
                    break
        else:
            for reserve_move in line.sale_line_id.reserve_stock_move_ids:
                lot_quants = reserve_move.reserved_quant_ids.filtered(lambda x:x.lot_id.id==line.lot_id.id)
                
                total_lot_qty = sum(x.qty for x in lot_quants)
                if total_lot_qty > total_dispatch_qty:
                    total_lot_qty = total_dispatch_qty
                    
                if reserve_move.product_uom_qty == total_dispatch_qty and total_lot_qty==total_dispatch_qty:
                    stock_move_ids+=reserve_move
                    total_dispatch_qty-=reserve_move.product_uom_qty
                    total_lot_qty -= reserve_move.product_uom_qty
                    line.sale_line_id.qty_reserved-=reserve_move.product_uom_qty
                    reserve_move.sale_order_line_id = line.sale_line_id.id
                    reserve_move.sale_order_id = line.sale_line_id.order_id.id
                    break
                
                elif reserve_move.product_uom_qty < total_lot_qty:
                    stock_move_ids+=reserve_move
                    total_dispatch_qty-=reserve_move.product_uom_qty
                    total_lot_qty -= reserve_move.product_uom_qty
                    line.sale_line_id.qty_reserved-=reserve_move.product_uom_qty
                    reserve_move.sale_order_line_id = line.sale_line_id.id
                    reserve_move.sale_order_id = line.sale_line_id.order_id.id
    
                else:
                    new_move = reserve_move.split(total_lot_qty)
                    new_move = self.env['stock.move'].browse(new_move)
                    
                    if reserve_move.reserved_quant_ids:
                        Quant = self.env['stock.quant']
                        quants = Quant.quants_get_preferred_domain(new_move.product_uom_qty, new_move, ops=False, lot_id=line.lot_id.id, domain=[('id','in',reserve_move.reserved_quant_ids.ids)], preferred_domain_list=[])
                        self.env['stock.quant'].quants_reserve(quants,new_move)
                    
                    line.sale_line_id.qty_reserved-=new_move.product_uom_qty
                    new_move.sale_order_line_id = line.sale_line_id.id
                    new_move.sale_order_id = line.sale_line_id.order_id.id                
                    stock_move_ids+=new_move
                    total_dispatch_qty-=total_lot_qty
                    total_lot_qty = 0
                    if total_dispatch_qty == 0: 
                        break

        return stock_move_ids,total_dispatch_qty
        
    @api.multi
    def create_unreserv_order(self):
        for rec in self:
            for line in rec.order_lines:
                if line.to_unreserve_qty > 0:
                    stock_move_ids = self.env['stock.move']
                    to_unreserve_qty = line.to_unreserve_qty
                    stock_move_ids,to_unreserve_qty = self.get_reserved_move_record(line,stock_move_ids,to_unreserve_qty)
                    stock_move_ids.action_cancel()
                    stock_move_ids.unlink()
                


class UnreserveSaleOrderLine(models.TransientModel):
    _name = "unreserve.order.line"
    _description = "Unreserve Sale Order"

    product_id = fields.Many2one('product.product', 'Product')
    unreserve_order_id = fields.Many2one('unreserve.order', 'Unreserve order')
    sale_order_id = fields.Many2one('sale.order', 'Sale Order')
    sale_line_id = fields.Many2one('sale.order.line', 'Sale Order Line')
    reserved_qty = fields.Float('Reserved')
    to_unreserve_qty = fields.Float('To Unreserve')
    order_qty = fields.Float('Order Qty')
    lot_id = fields.Many2one('stock.production.lot','Lot')
    line_number = fields.Integer('Line Number',default=0)
    
    @api.onchange('to_unreserve_qty')
    def oncahange_to_unreserve(self):
        if self.to_unreserve_qty < 0:
            self.to_unreserve_qty = 0
            return {'warning': {'title': _("Warning"), 'message': 'Not allowed to set negative quantity'}}
        
        if self.to_unreserve_qty > self.reserved_qty:
            self.to_unreserve_qty = 0
            msg = 'You only allowed to unreserve '+str(self.reserved_qty)+' quantity'
            return {'warning': {'title': _("Warning"), 'message': msg}}