# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Account Credit Debit note',
    'version' : '1.0',
    'summary': 'Manage the account credit and debit note',
    'description': """
    """,
    'category': 'Account',

    'depends' : ['account'],
    'data': [
        'views/account_invoice.xml',
    ],
    'demo': [

    ],
    'qweb': [

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}