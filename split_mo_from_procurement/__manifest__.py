# -*- coding: utf-8 -*-
{
    'name': "Split Manufacture order from procurement",

    'summary': """
        Set Configuration for Manufacture Order Quantity 
        """,

    'description': """When create manufacture order from procurement then if the MO is large then split it.
    """,


    'category': 'mrp',
    'version': '0.1',

    'depends': ['mrp'],

    'data': [
            'views/procurement_view.xml',
    ],

    'application' : True,
    'installable' : True,

}
