from odoo import fields, models, api, _


class MrpConfigSettings(models.TransientModel):
	_inherit = 'mrp.config.settings'

	limit_procurement_mo = fields.Float('Maximum MO Quantity', copy=False,default=0)

	@api.multi
	def set_limit_procurement_mo(self):
		return self.env['ir.values'].sudo().set_default(
			'mrp.config.settings', 'limit_procurement_mo', self.limit_procurement_mo)


class ProcurementOrder(models.Model):
	_inherit = 'procurement.order'

	@api.multi
	def create_new_procurementorder_qty(self, new_qty):
		new_procurement = self.copy({'product_qty': new_qty})
		self.product_qty = self.product_qty - new_qty
		if self.product_qty > new_qty:
			self.create_new_procurementorder_qty(new_qty)

	@api.multi
	def make_mo(self):
		for procurement in self:
			limit_procurement_mo = self.env['ir.values'].get_default('mrp.config.settings', 'limit_procurement_mo')
			new_qty = limit_procurement_mo
			if new_qty and new_qty > 0 and procurement.product_qty > new_qty:
				procurement.create_new_procurementorder_qty(new_qty)
		return super(ProcurementOrder, self).make_mo()
