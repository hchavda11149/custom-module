# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError

class StockPicking(models.Model):
    _inherit = "stock.picking"

    @api.multi
    def get_invoice_line_vals(self,pick,line):
        res = super(StockPicking,self).get_invoice_line_vals(pick,line)
        if pick.return_type == 'sale' and line.origin_returned_move_id and line.origin_returned_move_id.sale_order_line_id:
            res.update({'price_unit':line.origin_returned_move_id.sale_order_line_id.price_unit})
        return res
            
            
        
            
        