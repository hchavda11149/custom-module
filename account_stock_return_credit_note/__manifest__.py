# -*- coding: utf-8 -*-
{
    'name': "Account stock return debit note",

    'summary': """
        Create debit note when stock is return""",

    'description': """
        This module is used for create debit note when stock is return from stock picking
    """,

    'category': 'Stock',
    'version': '0.1',

    'depends': ['account_stock_return_note','sale_stock_pending_reservation'],

    'data': [
    ],

    'application': True,
    'installable': True,
}
