# -*- coding: utf-8 -*-
{
    'name': "Work Center Location Link",

    'summary': """Work Center Location Linking Management to show the product stock for particular workcenter""",

    'description': """
        
    """,

    'category': 'Manufacturing',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['mrp','stock'],

    # always loaded
    'data': [
       'views/workcenter_location.xml',

    ],
    # only loaded in demonstration mode
    'demo': [],
    'installable': True,
    'application': True,
}
