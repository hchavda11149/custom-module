from odoo import api, fields, models, _

class WorkCenterLocation(models.Model):
    _inherit='mrp.workcenter'
    
    workcenter_location = fields.Many2one('stock.location','Work Center Location')