from openerp import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class ReceivedContractProduct(models.TransientModel):
	_name = 'received.contract.product'

	partner_id = fields.Many2one('res.partner', 'Vendor')
	received_contract_ids = fields.One2many('received.contract.product.line', 'received_contract_id')
	subcontract_id = fields.Many2one('mrp.subcontract', 'Subcontract')

	@api.multi
	def create_received_history(self, line):

		vals = {
			'subcontract_id': self.subcontract_id and self.subcontract_id.id or False,
			'qty': line.qty,
			'reject_qty': line.qty_reject,
			'production_id': line.mo_id and line.mo_id.id or False,
			'workorder_id': line.workorder_id and line.workorder_id.id or False,
			'weight_kg': line.weight_kg,
			'subcontract_line_id': line.contract_line_id.id,
		}
		self.env['received.subcontract.history'].create(vals)


	@api.multi
	def process_mo_qty(self,line):
		if line.mo_id:
			line.mo_id.action_assign()
			if line.mo_id.post_visible:
				line.mo_id.post_inventory()
			if line.mo_id.check_to_done:
				line.mo_id.button_mark_done()

	@api.multi
	def get_lot_for_product(self,product,workorder_id,lot_id):
		if lot_id:
			return lot_id
		elif workorder_id and workorder_id.final_lot_id:
			return workorder_id.final_lot_id
		else:
			return self.env['stock.production.lot'].create({'product_id' : product.id,
													'product_uom_id' : product.uom_id and product.uom_id.id or False 
													})		
	@api.multi
	def process_workorder_qty(self,line):
		if (line.workorder_id.production_id.product_id.tracking != 'none'):
			if line.workorder_id.production_id.product_id.tracking=='lot':
				lot_id = self.get_lot_for_product(line.workorder_id.production_id.product_id,line.workorder_id,False)
				line.workorder_id.final_lot_id = lot_id.id
				line.workorder_id.qty_producing = line.qty
				line.workorder_id._onchange_qty_producing()
				line.workorder_id.record_production()
			else:
				for count in range(int(line.qty)):
					lot_id = self.get_lot_for_product(line.workorder_id.production_id.product_id,line.workorder_id,False)
					line.workorder_id.final_lot_id = lot_id.id
					line.workorder_id._onchange_qty_producing()
					line.workorder_id.record_production()
					self.process_mo_qty(line)
							
		else:
			line.workorder_id.qty_producing = line.qty
			line.workorder_id._onchange_qty_producing()
			line.workorder_id.record_production()
		
				
	@api.multi
	def received_quantity(self):
		self.ensure_one()
		for line in self.received_contract_ids:
			if line.contract_line_id.pending_qty < line.qty + line.qty_reject:
				raise UserError(_("You are only allowed to receive %d Quantity of product %s") % (
				line.contract_line_id.pending_qty, line.mo_id.product_id.semi_finished_product_id.name))
			line.contract_line_id.received_qty += line.qty
			line.contract_line_id.reject_qty += line.qty_reject
			if line.workorder_id:
				if line.workorder_id.state != 'progress':
					line.workorder_id.button_start()
				if line.workorder_id.qty_producing < line.qty:
					if line.workorder_id.qty_produced > 0:
						new_mo_qty = line.workorder_id.qty_produced + line.qty
					else:
						new_mo_qty = line.qty
					change_production = self.env['change.production.qty'].create(
						{'mo_id': line.workorder_id.production_id.id,
						 'product_qty': new_mo_qty,
						 })
					change_production.change_prod_qty()
				
				self.process_workorder_qty(line)	
			self.process_mo_qty(line)
			self.create_received_history(line)
		if self.subcontract_id.is_received:
			self.subcontract_id.action_received_state()
		return True


class ReceivedContractProductLine(models.TransientModel):
	_name = 'received.contract.product.line'

	@api.depends('qty_accept', 'qty_rework', 'qty_accept_ud')
	def _get_done_qty(self):
		for record in self:
			record.qty = record.qty_accept + record.qty_accept_ud + record.qty_rework + record.qty

	product_id = fields.Many2one('product.product', string='Product')
	qty = fields.Float(string='QTY Done', compute='_get_done_qty', store=True)
	product_uom = fields.Many2one('product.uom', string='Unit of Measure')
	received_contract_id = fields.Many2one('received.contract.product', 'Received Contract')
	contract_line_id = fields.Many2one('mrp.subcontract.lines', 'Contract Line')
	weight_kg = fields.Float('KGS')
	mo_id = fields.Many2one('mrp.production', 'Production')
	workorder_id = fields.Many2one('mrp.workorder', 'Workorder')
	rate = fields.Float('Rate')
	qty_accept = fields.Float('Qty Accepted')
	qty_accept_ud = fields.Float('Qty Accepted UD')
	qty_rework = fields.Float('Rework')
	qty_reject = fields.Float('Reject Qty.')
	remaining_qty = fields.Float('Remaining Qty.')
	reject_type = fields.Many2one('reject.reason', string="Rejection Type")
