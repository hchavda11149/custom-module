# -*- coding: utf-8 -*-
{
    'name': "MRP Subcontract",

    'summary': """
        General Module For Subcontracting""",

    'description': """
        This module is used for subcontracting or jobwork for outside the company.
    """,


    'category': 'Manufacture',
    'version': '0.1',

    'depends': ['base','mrp','stock','quality_rejection','mrp_set_previous_workorder'],

    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'data/sequence.xml',
        'views/mrp_subcontract_views.xml',
        #'views/stock_location_view.xml',
        'wizard/received_subcontract_product.xml',
    ],

    'application': True,
    'installable': True,
}
