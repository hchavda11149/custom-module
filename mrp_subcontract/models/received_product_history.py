from odoo import models,fields,api,_
from datetime import datetime

class Received_Subcontract_History(models.Model):
    
    _name = 'received.subcontract.history'
    
    subcontract_id = fields.Many2one('mrp.subcontract')
    qty = fields.Float('Quantity')
    reject_qty = fields.Float('Reject Quantity')
    production_id = fields.Many2one('mrp.production','Batch No.')
    workorder_id = fields.Many2one('mrp.workorder','Process')
    weight_kg = fields.Float('Kgs.')
    subcontract_line_id = fields.Many2one('mrp.subcontract.lines','Line Id')
    delivery_date = fields.Date('Delivery Date',default=datetime.now())  