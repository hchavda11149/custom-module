from odoo import models, fields, api


class StockLocation(models.Model):
	_inherit = 'stock.location'

	is_subcontract_location = fields.Boolean('Is a Subcontract Location?', copy=False, default=False)
