# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import UserError, ValidationError


class MrpSubcontract(models.Model):
    _name = 'mrp.subcontract'
    _order = "id desc"
    _rec_name = 'challan_no'
    
    partner_id = fields.Many2one('res.partner', 'Party')
    company_id = fields.Many2one('res.company', 'Company',
    							 default=lambda self: self.env['res.company']._company_default_get('mrp.subcontract'))
    challan_no = fields.Char("Challan No.")
    date = fields.Date(string="Order Date", default=datetime.now(), track_visibility='onchange')
    delivery_date = fields.Date(string="Delivery Date", track_visibility='onchange')
    disc_goods = fields.Text("Description")
    process = fields.Char("Process")
    duration_of_process = fields.Char("Duration of Process")
    id_marks = fields.Char("Marks & Number")
    hsn_sac = fields.Char("HSN / SAC")
    process_place = fields.Char("Place")
    vnd_gst_num = fields.Char("GSTIN No.")
    line_ids = fields.One2many('mrp.subcontract.lines', 'line_id', string="Subcontract Info")
    state = fields.Selection(
    	[('draft', 'Draft'), ('in_process', 'In Process'), ('received', 'Received'), ('cancel', 'Cancel')],
    	default='draft', track_visibility='onchange')
    received_subcontract_history_ids = fields.One2many('received.subcontract.history', 'subcontract_id',
    												   string='History')
    is_received = fields.Boolean(compute="check_received_picking", string='Received Picking')
    supplier_challan_no = fields.Char('Supplier Challan No', copy=False)
    supplier_challan_date = fields.Date('Supplier Challan Date', copy=False)
    is_invoice_done = fields.Boolean(string='Invoice Done', store=True)
    count_after_delivery = fields.Integer('After Delivery')
    count_before_delivery = fields.Integer('Before Delivery')
    total_delivery = fields.Integer('Total Delivery')

    @api.model
    def create(self, vals):
        seq = self.env['ir.sequence'].next_by_code('mrp.subcontract')
        vals['challan_no'] = seq
        res = super(MrpSubcontract, self).create(vals)
        return res

    @api.multi
    def unlink(self):
        for state_id in self:
            if state_id.state == 'received':
                raise UserError(_('You cannot delete this Subcontract'))
        return super(MrpSubcontract, self).unlink()

    @api.depends('line_ids.is_received')
    def check_received_picking(self):
        for order in self:
            is_received = True
            for line in order.line_ids:
                if not line.is_received:
                    is_received = False
                    return
            order.is_received = is_received

    @api.multi
    def action_received_state(self):
        self.state = 'received'
        
    @api.multi
    def button_inprogress(self):
        for record in self:
            previous_line_product = 0.0
            for line in record.line_ids:
                if line.qty <= 0:
                    raise UserError(_("You are not allowed to process 0 quantity."))
                previous_produce_qty = 0.0
                actual_produce_qty = line.qty
                if line.workorder_id and line.workorder_id.previous_workorder_id:
                    previous_produce_qty = line.workorder_id.previous_workorder_id.qty_produced - line.workorder_id.qty_produced
                previous_produce_qty += previous_line_product
                if line.workorder_id.previous_workorder_id and previous_produce_qty < actual_produce_qty:
                    raise UserError(_("Product quantity not available. Please first process %s") % (
						line.workorder_id.previous_workorder_id.name))
                previous_line_product = actual_produce_qty
            record.state = 'in_process'
        return True

    @api.multi
    def button_received(self):
        self.ensure_one()
        form_view = self.env.ref('mrp_subcontract.wizard_received_subcontract_product_view')
        val = []
        for line in self.line_ids:
            if line.qty != line.received_qty:
                val.append((0, 0, {
        			'product_id': line.product_id.id,
        			'qty_accept': line.qty - line.received_qty,
        			# 'product_uom':line.product_uom.id,
        			'contract_line_id': line.id,
        			'mo_id': line.production_id and line.production_id.id or False,
        			'workorder_id': line.workorder_id and line.workorder_id.id or False,
        			'weight_kg': line.weight_kg
        		}))
        return {
        	'name': _('Receive Product'),
        	'view_type': 'form',
        	'view_mode': 'form',
        	'res_model': 'received.contract.product',
        	'target': 'new',
        	'view_id': form_view.id,
        	'type': 'ir.actions.act_window',
        	'context': {'default_subcontract_id': self.id, 'default_partner_id': self.partner_id.id,
        				'default_received_contract_ids': val}
        }

    @api.multi
    def cancel_subcontract(self):
        for record in self:
            record.state = 'cancel'
        return True

	def get_move_vals(self, product_id, rate, qty, location_id, dest_location_id, mo_id):
		##### Create stock move value when the semi finished sub contract process done
		vals = {}
		if location_id and dest_location_id:
			vals = {
				'product_id': product_id.id,
				'product_uom_qty': qty,
				'product_uom': product_id.uom_id.id,
				'name': product_id.name,
				'location_id': location_id.id,
				'location_dest_id': dest_location_id.id,
				'scrapped': 0,
				'state': 'confirmed',
				'price_unit': rate,
				'sub_mo_id': mo_id
			}
		return vals

class MrpSubcontractLine(models.Model):
    
    _name = 'mrp.subcontract.lines'
    line_id = fields.Many2one('mrp.subcontract', ondelete='cascade')
    
    production_id = fields.Many2one('mrp.production', string="Batch No")
    workorder_id = fields.Many2one('mrp.workorder', string="Process")
    product_id = fields.Many2one('product.product', string="Product")
    product_uom = fields.Many2one('product.uom', string='UOM')
    qty = fields.Float(string='Qty.')
    weight_kg = fields.Float(string='Kgs.')
    rate = fields.Float('Rate')
    received_qty = fields.Float('Received Qty')
    reject_qty = fields.Float('Reject Qty')
    pending_qty = fields.Float(compute='get_pending_qty', string='Pending Qty')
    is_received = fields.Boolean(compute="check_received_picking", string='Received')
    is_invoice_done = fields.Boolean('Invoice Done', copy=False, default=False)
    
    @api.onchange('production_id')
    def onchange_production_id(self):
        if self.production_id:
            self.product_id = self.production_id.product_id.id
            self.workorder_id = False
        else:
            self.product_id = False


    @api.onchange('workorder_id')
    def onchange_workorder_id(self):
        if self.workorder_id:
            if not self.workorder_id.previous_workorder_id:
                if self.workorder_id.product_id.tracking == 'serial':
                    self.qty = self.workorder_id.qty_production - self.workorder_id.qty_produced
                else:
                    self.qty = self.workorder_id.qty_producing
    
            elif self.workorder_id.previous_workorder_id and self.workorder_id.previous_workorder_id.state == 'done':
                self.qty = self.workorder_id.qty_production - self.workorder_id.qty_produced
            elif self.workorder_id.previous_workorder_id:
                self.qty = self.workorder_id.previous_workorder_id.qty_produced - self.workorder_id.qty_produced
            else:
                self.qty = 0
        else:
            self.qty = 0

    @api.multi
    def get_pending_qty(self):
        for line in self:
            line.pending_qty = line.qty - (line.received_qty + line.reject_qty)

    @api.depends('received_qty', 'reject_qty')
    def check_received_picking(self):
        for line in self:
            if line.received_qty + line.reject_qty == line.qty:
                line.is_received = True
    
