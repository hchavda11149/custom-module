# -*- coding: utf-8 -*-
{
    'name': "Customer PO No And Date",

    'summary': """
       Add fields for Customer Po Number and Date into Sale order""",


    'category': 'stock',
    'version': '0.1',
    'depends': ['sale'],
    'data': [
             
             'views/sale_po.xml'
            
             ],
    'installable': True,
    
    'application': True,
}
