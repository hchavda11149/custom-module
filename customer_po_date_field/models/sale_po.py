
from odoo import models, fields, api, _

class SaleOrderInherit(models.Model):
    _inherit = 'sale.order'

    customer_po_no = fields.Char("Customer PO No")
    customer_po_date = fields.Date('PO Date')

