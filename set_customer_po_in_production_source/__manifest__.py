# -*- coding: utf-8 -*-
{
    'name': "Set Sale order customer PO in production source",

    'summary': """
         When Create Manufacture order base on sale order then set the sale order reference into the Manufacture order 
        """,

    'description': """
    """,

    'category': 'mrp',
    'version': '0.1',

    'depends': ['mrp','deltatech_mrp_sale_ref'],

    'data': [
            
    ],

    'application' : True,
    'installable' : True,

}
