from odoo import models,fields,api,_
from odoo.exceptions import UserError, ValidationError

class MRPProduction(models.Model):
    
    _inherit = 'mrp.production'

    @api.model
    def create(self,vals):
        res = super(MRPProduction,self).create(vals)
        if res.procurement_ids and res.sale_order_id:
            if res.sale_order_id.customer_po_no:
                res.origin = res.sale_order_id.customer_po_no
        return res
