# Part of Flectra See LICENSE file for full copyright and licensing details.

import time
from odoo import api, fields, models, _
import re
from odoo.exceptions import MissingError, UserError, ValidationError

class ResCompany(models.Model):
    
    _inherit = 'res.company'

    gstin_number = fields.Char('GSTIN Number')
    
    gst_type = fields.Selection([('regular', 'Regular'),
                                 ('unregistered', 'Unregistered'),
                                 ('composite', 'Composite'),
                                 ('volunteer', 'Volunteer')],
                                string='GST Type',
                                related='partner_id.gst_type')
     
    gst_introduce_date = fields.Date(string='GST Introduce Date',
                                     default=time.strftime('2017-07-01'))
 
 
    company_b2c_limit_line = fields.One2many('res.company.b2c.limit',
                                             'company_id', string='B2C Limit')
 
    rc_gst_account_id = fields.Many2one('account.account', 'Reverse Charge') 
    
    @api.onchange('gst_type')
    def onchange_gst_type(self):
        """ If gst type is unregistered then GSTIN Number should be blank"""
        if self.gst_type == 'unregistered':
            self.gstin_number = False


    @api.multi
    def check_gstin_number_pattern(self,number):
        pattern = "^\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}Z[A-Z\d]{1}$"
        check_pattern = re.match(pattern,number)

        if not check_pattern:
            raise ValidationError("Invalid GSTIN Number")

    @api.multi
    def write(self,vals):
        for company in self:
            if 'gstin_number' in vals and vals.get('gstin_number'):
                self.check_gstin_number_pattern(vals.get('gstin_number'))
        return super(ResCompany,self).write(vals)

    @api.model
    def create(self, vals):
        if 'gstin_number' in vals and vals.get('gstin_number'):
            self.check_gstin_number_pattern(vals.get('gstin_number'))
        return super(ResCompany,self).create(vals)


class CompanyB2CLimit(models.Model):
    _name = 'res.company.b2c.limit'

    date_from = fields.Date(string='From')
    date_to = fields.Date(string='To')
    b2cl_limit = fields.Float(string='B2CL Limit', default=250000.0,
                              help='Inter state minimum limit for B2CL type '
                                   'transactions.')
    b2cs_limit = fields.Float(string='B2CS Limit', default=250000.0,
                              help='Inter state maximum limit for B2CS type '
                                   'transactions.')
    company_id = fields.Many2one('res.company', string='Company')

    @api.constrains('date_to', 'date_from', 'company_id')
    def _check_sheet_date(self):
        for line in self:
            self.env.cr.execute('''
                    SELECT id
                    FROM res_company_b2c_limit
                    WHERE (date_from <= %s and %s <= date_to)
                        AND company_id=%s
                        AND id <> %s''',
                                (line.date_to, line.date_from,
                                 line.company_id.id, line.id))
            if any(self.env.cr.fetchall()):
                raise ValidationError(_(
                    'You cannot have 2 limit lines of same period that '
                    'overlap for %s!') % (line.company_id and
                                          line.company_id.name))

    @api.constrains('date_from', 'date_to')
    def _check_dates(self):
        if any(self.filtered(lambda line: line.date_from and line.
                             date_to and line.date_from > line.date_to)):
            raise ValidationError(_(
                'From date must be lower than to date.'))

