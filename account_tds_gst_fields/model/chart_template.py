from odoo.exceptions import AccessError
from odoo import api, fields, models, _
from odoo import SUPERUSER_ID

import logging
_logger = logging.getLogger(__name__)

class AccountTaxTemplate(models.Model):
    
    _inherit = 'account.tax.template'
    
    is_tds = fields.Boolean('TDS',copy=False,default=False)
    tax_type = fields.Selection(selection=[('cgst', 'CGST'), ('sgst', 'SGST'), ('igst', 'IGST')], string="Tax Type")
    
    def _get_tax_vals(self, company):
        """ This method generates a dictionnary of all the values for the tax that will be created.
        """
        self.ensure_one()
        val = super(AccountTaxTemplate,self)._get_tax_vals(company)
        val.update({
                    'is_tds':self.is_tds,
                    'tax_type':self.tax_type,
                    })
        return val
    
    
class AccountAccountTemplate(models.Model):
    
    _inherit = "account.account.template"
    
    is_tds = fields.Boolean('TDS Account')
    is_gst = fields.Boolean('GST Account')


class AccountChartTemplate(models.Model):
    
    _inherit = "account.chart.template"
    
    def _get_account_vals(self, company, account_template, code_acc, tax_template_ref):
        self.ensure_one()
        val = super(AccountChartTemplate,self)._get_account_vals(company, account_template, code_acc, tax_template_ref)
        val.update({
                    'is_tds':account_template.is_tds,
                    'is_gst':account_template.is_gst,
                    })
        return val

