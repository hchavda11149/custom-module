from odoo import models,fields,api,_

class AccountTax(models.Model):
    
    _inherit = "account.tax"
    
    is_tds = fields.Boolean('TDS',copy=False,default=False)
    is_tcs = fields.Boolean('TCS',copy=False,default=False)
    tax_type = fields.Selection(selection=[('cgst', 'CGST'), ('sgst', 'SGST'), ('igst', 'IGST'),('cess','CESS')], string="Tax Type")