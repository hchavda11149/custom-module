from odoo import models,fields,_

class PortCode(models.Model):

    _name = 'port.code'

    name = fields.Char('Port Name',required=True)
    code = fields.Char('Port Code',required=True)

