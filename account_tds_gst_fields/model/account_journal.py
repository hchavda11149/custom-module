from odoo import models,fields,api,_

class AccountJournal(models.Model):
    
    _inherit = "account.journal"
    
    is_export_journal = fields.Boolean('Export Journal',copy=False,default=False)
    consider_in_sales_report = fields.Boolean('Consider In Sales Report', copy=False, default=False)
    
