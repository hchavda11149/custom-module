from odoo import models,fields,api,_

class AccountAccount(models.Model):
    
    _inherit = "account.account"
    
    is_tds = fields.Boolean('TDS Account')
    is_tcs = fields.Boolean('TCS Account')
    is_gst = fields.Boolean('GST Account')
