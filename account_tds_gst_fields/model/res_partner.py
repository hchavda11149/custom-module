from odoo import models,fields,_,api
import re
from odoo.exceptions import MissingError, UserError, ValidationError

class ResPartner(models.Model):

    _inherit = 'res.partner'
    
    gstin_number = fields.Char('GSTIN Number')
    pan_no = fields.Char('PAN', help='PAN Number')
    cin_no = fields.Char('CIN No')
    gst_type = fields.Selection([('regular', 'Regular'),
                                 ('unregistered', 'Unregistered'),
                                 ('composite', 'Composite'),
                                 ('volunteer', 'Volunteer')],
                                string='GST Type')

    @api.onchange('gst_type')
    def onchange_gst_type(self):
        """
        If gst type is unregistered then GSTIN Number should be blank
        """
        if self.gst_type == 'unregistered':
            self.gstin_number = False
            
    def _get_partner_location_details(self, company):
        partner_location = False
        if self.country_id and company.country_id:
            partner_location = 'inter_country'
            if self.country_id.id == company.country_id.id:
                partner_location = 'inter_state'
                if self.state_id and company.state_id and self.state_id.id == \
                        company.state_id.id:
                    partner_location = 'intra_state'
        return partner_location
