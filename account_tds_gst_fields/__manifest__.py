# -*- coding: utf-8 -*-
{
    'name': "Account TDS GST Fields",

    'summary': """ Common TDS GST Fields """,

    'description': """ Common TDS GST Fields """,

    'category': 'accounting',

    'version': '0.1',

    'depends': ['account'],

    'data': [
                 'security/ir.model.access.csv',
                'views/account_tax_view.xml',
                'views/account_account_view.xml',
                'views/res_company.xml',
                'views/res_partner.xml',
                'views/account_invoice.xml',
                'views/account_journal.xml',
            ],
    
    'demo': [ ],
    
    'installable': True,
    
    'application': True,
}
