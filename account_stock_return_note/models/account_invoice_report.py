from odoo import models, fields, api

class AccountInvoiceReport(models.Model):
    _inherit = 'account.invoice.report'

    def _from(self):
        return super(AccountInvoiceReport, self)._from() + " AND ai.id not in (select id from account_invoice where credit_note_type='without_stock' and type='out_refund')"
