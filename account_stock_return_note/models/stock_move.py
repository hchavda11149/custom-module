from odoo import models,fields,api,_

class StockMove(models.Model):
    
    _inherit = 'stock.move'
    
    is_return_refund = fields.Boolean('Is Refund',copy=False,default=False)
    
    