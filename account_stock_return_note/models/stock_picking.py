# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError


class StockOperationType(models.Model):
    _inherit = 'stock.picking.type'
    
    return_type = fields.Selection([('sale','Purchase Return'),('purchase','Sale Return')],string='Return Type')

    
class StockPicking(models.Model):
    _inherit = "stock.picking"
    
    return_type = fields.Selection([('sale','Purchase Return'),('purchase','Sale Return')],string='Return Type',related='picking_type_id.return_type',readonly=True,store=True)
    invoice_id = fields.Many2one('account.invoice',string="Invoice")
    
    def action_view_return_invoice(self):
        if self.return_type =='sale':
            action = self.env.ref('account_credit_debit.action_credit_invoice_tree1').read()[0]
            action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
        else:
            action = self.env.ref('account_credit_debit.action_debit_invoice_tree2').read()[0]
            action['views'] = [(self.env.ref('account.invoice_supplier_form').id, 'form')]
        action['res_id'] = self.invoice_id.id
        return action
    
    @api.multi
    def get_invoice_line_vals(self,pick,line):
        name = line.product_id.partner_ref
        price_unit = line.price_unit
    
        return {
            'product_id':line.product_id.id,
            'name':name,
            'quantity':line.product_uom_qty,
            'price_unit': price_unit,
            'account_id':line.product_id.product_tmpl_id.get_product_accounts()['income'].id}
        
    @api.multi   
    def create_refund_invoice(self):   
        inv_obj = self.env['account.invoice']
        for pick in self.filtered(lambda x:x.return_type):
            if not pick.invoice_id:
                type = 'in_refund' if pick.return_type == 'purchase' else 'out_refund'
                inv_lines = {'credit_note_type':'with_stock','type':type, 'partner_id':pick.partner_id.id, 'invoice_line_ids':[]}
                account = pick.return_type == 'sale' and pick.partner_id.property_account_receivable_id.id or pick.partner_id.property_account_payable_id.id
                inv_lines['account_id'] = account
                inv_lines['origin'] = pick.name
                inv_lines['name'] = pick.origin
                for line in pick.move_lines.filtered(lambda x:x.is_return_refund):
                    line_vals = self.get_invoice_line_vals(pick,line) 
                    inv_lines['invoice_line_ids'] += [(0, None,line_vals)]
                if inv_lines['invoice_line_ids']:
                    inv_id = inv_obj.create(inv_lines)
                    pick.invoice_id = inv_id.id

    @api.multi
    def do_transfer(self):
        res = super(StockPicking, self).do_transfer()
        self.create_refund_invoice()
        return res
   
