# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AccountInvoice(models.Model):
	_inherit = "account.invoice"

	credit_note_type = fields.Selection([
										('with_stock', 'With Stock'),
										('without_stock', 'Without Stock')
										], string="Credit Note Type", default="without_stock")