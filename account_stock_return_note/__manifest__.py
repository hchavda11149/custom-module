# -*- coding: utf-8 -*-
{
    'name': "Account stock return credit note",

    'summary': """
        Create credit note when stock is return""",

    'description': """
        This module is used for create credit note when stock is return from stock picking
    """,

    'category': 'Stock',
    'version': '0.1',

    'depends': ['base','account','stock','account_credit_debit'],

    'data': [
        'views/credit_note_view.xml',
        'views/stock_picking_view.xml',
    ],

    'application': True,
    'installable': True,
}
