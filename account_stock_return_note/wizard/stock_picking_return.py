from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError


class ReturnPicking(models.TransientModel):
    _inherit = 'stock.return.picking'
    _description = 'Return Picking'

    @api.multi
    def _create_returns(self):
        
        new_picking, picking_type_id = super(ReturnPicking,self)._create_returns()
        new_picking = self.env['stock.picking'].browse(new_picking)
        
        for return_line in self.product_return_moves.filtered(lambda x:x.to_refund_so):
            move_id = new_picking.move_lines.filtered(lambda x:x.origin_returned_move_id.id==return_line.move_id.id)
            if move_id:
                move_id[0].is_return_refund = True
            
        return new_picking.id, picking_type_id 