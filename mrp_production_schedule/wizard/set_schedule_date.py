from odoo import api, fields, models


class SetScheduleDate(models.TransientModel):
	_name = 'set.schedule.date'

	line_ids = fields.One2many('set.schedule.date.line', 'wizard_id', 'Lines')

	@api.model
	def default_get(self, fields):
		res = super(SetScheduleDate, self).default_get(fields)
		workorder_ids = self.env['mrp.workorder'].browse(self.env.context.get('active_ids'))
		vals = []
		for workorder in workorder_ids:
			data_vals = {
						 'workorder_id': workorder.id,
						 'production_id': workorder.production_id.id,
						}
			vals.append((0, 0, data_vals))
		res.update({
					'line_ids': vals,
					})
		return res

	@api.multi
	def get_production_planning_data(self,line):
		return {
			'production_id': line.production_id and line.production_id.id or False,
			'workorder_id': line.workorder_id and line.workorder_id.id or False,
			'machine_id': line.machine_id and line.machine_id.id or False,
			'schedule_date': line.schedule_date,
		}
	@api.multi
	def set_date(self):
		for line in self.line_ids:
			vals = self.get_production_planning_data(line)
			self.env['production.planning.schedule'].create(vals)


class SetScheduleDateLine(models.TransientModel):

	_name = 'set.schedule.date.line'

	wizard_id = fields.Many2one('set.schedule.date', 'Wizard')
	workorder_id = fields.Many2one('mrp.workorder', 'Workorder')
	production_id = fields.Many2one('mrp.production', 'MO')
	schedule_date = fields.Date("Scheduled Date")
	machine_id = fields.Many2one('maintenance.equipment', 'Machine')
