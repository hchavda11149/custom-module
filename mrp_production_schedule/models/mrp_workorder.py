from odoo import models, fields, api


class MRPWorkorder(models.Model):
	_inherit = 'mrp.workorder'

	planning_lines = fields.One2many('production.planning.schedule', 'workorder_id', 'Planning Lines')


class ProductionPlanningSchedule(models.Model):
	_name = 'production.planning.schedule'

	production_id = fields.Many2one('mrp.production', 'MO')
	workorder_id = fields.Many2one('mrp.workorder', 'Workorder')
	schedule_date = fields.Date("Scheduled Date")
	machine_id = fields.Many2one('maintenance.equipment', 'Machine')
