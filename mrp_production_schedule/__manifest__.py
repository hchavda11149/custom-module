# -*- coding: utf-8 -*-
{
    'name': "MRP Production Schedule",

    'summary': """
        Set Schedule Date In Production Workorder
        """,

    'description': """
        This module helps you to set schedule date in production.So user can see where the production is start for that process 
    """,


    'category': 'Manufacturing',
    'version': '0.1',

    'depends': ['mrp','mrp_workorder','mrp_maintenance'],

    'data': [
        'security/ir.model.access.csv',
        'views/mrp_workorder_view.xml',
        'wizard/set_schedule_date_view.xml',
    ],

    'application': True,
    'installable': True,
}
