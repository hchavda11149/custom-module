from odoo import models,fields,api,_

class MrpProduction(models.Model):

    _inherit = 'mrp.production'
    
    def _workorders_create(self, bom, bom_data):
        """
            Set the previous work order id into the current work order
        """
        workorders = super(MrpProduction,self)._workorders_create(bom=bom,bom_data=bom_data)
        previous_workorder_id = False 
        for workorder in workorders:
            if previous_workorder_id:
                workorder.previous_workorder_id = previous_workorder_id.id
            previous_workorder_id = workorder
            
        return workorders