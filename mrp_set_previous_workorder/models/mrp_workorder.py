from odoo import models,fields,api,_

class MRPWorkorder(models.Model):
    
    _inherit = 'mrp.workorder'
    
    previous_workorder_id = fields.Many2one('mrp.workorder',string='Previos Workorder')
    qty_production = fields.Float('Original Production Quantity', readonly=True, related='production_id.product_qty',
                                  store=True)
    is_last_process = fields.Boolean(string='Is Last Process',compute='_get_last_process',store=True,copy=False)

    @api.multi
    def _get_last_process(self):
        for record in self:
            if not record.next_work_order_id:
                record.is_last_process = True
            else:
                record.is_last_process = False
