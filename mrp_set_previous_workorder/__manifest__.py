# -*- coding: utf-8 -*-

{
    'name': "Set Previous Workorder",
    "summary": """When create production with work order then set previous Work Order into current Work Order.
				base on the we can track the production qty of previous work order""",
    'category': 'MRP',
    'version': '1.0',

    'depends': ['mrp'],

    'data': [
    ],

    'application' : True,
    'installable' : True,

}
