from odoo import models,fields,api,_

class StockMove(models.Model):
    
    _inherit = 'stock.move'
    
    @api.multi
    def delete_stock_move(self):
        self.action_cancel()
        self.unlink()    