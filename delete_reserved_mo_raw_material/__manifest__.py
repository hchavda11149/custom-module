# -*- coding: utf-8 -*-
{
    'name': "Delete Reserved Raw Material From The MO",

    'summary': """
        This module used for the delete the raw material manually from the MO.
        """,

    'description': """
        
    """,

    'category': 'Manufacturing',
    'version': '0.1',

    'depends': ['mrp'],

    'data': [
             'view/mrp_production.xml',
    ],
    'application': True,
    'installable': True,
}
