
{
    'name': 'Account Invoice Other Features',
    'version': '1.0',
    'category': 'Account',
    'sequence': 1,
    'summary': '',
    'description': """
        Add functionality for invoice round off value and packing cost on invoice on invoice,
        Also TDS and TCS tax for the bill. 
    """,

    'website': '',
    'depends': ['account','account_tds_gst_fields'],
    'data': [
             'security/ir.model.access.csv',
             'data/account_data.xml',
             'view/account_config_setting.xml',
             'view/account_invoice.xml',
             #'view/res_partner_view.xml',
             'view/account_tax.xml',
             'view/account_patment_view.xml',
             'wizard/tds_wizard_report_view.xml',
             ],   
    'events': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
