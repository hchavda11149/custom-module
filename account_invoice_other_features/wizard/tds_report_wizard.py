from odoo import models,fields,api,_
import time
from datetime import datetime
from dateutil import relativedelta
from openerp.exceptions import Warning
import base64
from xlwt import easyxf
import xlwt
from cStringIO import StringIO
        
class TDSReportWizard(models.Model):
    
    _name = 'tds.report.wizard'
    
    from_date = fields.Date('From Date',default= lambda *a: time.strftime('%Y-%m-01'))
    to_date = fields.Date('To Date',default= lambda *a: str(datetime.now() + relativedelta.relativedelta(months=+1,day=1, days=-1))[:10])
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id.id)

    @api.multi
    def print_excel_report(self):
        self.ensure_one()
        if self.from_date > self.to_date:
            raise Warning(_('From date is bigger then to date'))
        filename='TDS_Report.xls'
        workbook = xlwt.Workbook()
        style = xlwt.XFStyle()
       
        style1_left=easyxf('font: name Arial;''alignment: horizontal  left;''font:bold True,height 200;')
        style1_sub_total=easyxf('font: name Arial;''alignment: horizontal  right;''font:bold True,height 200;')
        style_title_center=easyxf('font: name Arial;''alignment: horizontal  center;''font:bold True,height 220;')
        style1_total=easyxf('font: name Arial;''alignment: horizontal  right;''font:bold True,height 200;''border: left thin,right thin,top thin,bottom thin;')
        
        font = xlwt.Font()
        font.name = 'Times New Roman'
        font.bold = True
        font.height = 250
        style.font = font
        worksheet = workbook.add_sheet('Sheet 1')
        row = 0
        col = 0
        worksheet.col(1).width=256*45
        worksheet.col(2).width=256*15
        worksheet.col(3).width=256*15
        worksheet.write_merge(row,row,col,3, "TDS Payable Report",style_title_center)
        row += 1
        from_date =  datetime.strptime(self.from_date,'%Y-%m-%d').strftime('%d-%m-%Y')
        to_date =  datetime.strptime(self.to_date,'%Y-%m-%d').strftime('%d-%m-%Y')
        worksheet.write_merge(row,row,col,3, "Date : %s To %s" %(from_date,to_date),style1_left)
        row += 1
        col = 0
        worksheet.write(row,col, "Serial No",style1_left)
        col += 1
        worksheet.write(row,col, "Vendor",style1_left)
        col += 1
        worksheet.write(row,col, "PAN Card No",style1_left)
        col += 1
        worksheet.write(row,col, "Reference",style1_left)
        col += 1
        worksheet.write(row,col, "Date",style1_left)
        col+=1
        worksheet.write(row,col, "Amount",style1_left)
        col+=1
        worksheet.write(row,col, "Total",style1_left)
        col+=1 

        row+=1
        
        accounts = self.env['account.account'].search([('is_tds','=',True)]).ids
        
        if accounts:
            tables, where_clause, where_params = self.env['account.move.line'].with_context(strict_range=True,date_from=self.from_date,date_to=self.to_date,company_id=self.company_id.id)._query_get()
            tables = tables.replace('"','')
            if not tables:
                tables = 'account_move_line'
            wheres = [""]
            if where_clause.strip():
                wheres.append(where_clause.strip())
            filters = " AND ".join(wheres)
            # compute the balance, debit and credit for the provided accounts
            request = ("SELECT partner_id AS partner_id,date as date,move_id as move_id,id,ref as ref,debit AS debit, credit AS credit, (debit - credit) AS balance" +\
                       " FROM " + tables + " WHERE account_id IN %s " + filters)
            params = (tuple(accounts),) + tuple(where_params)
            self.env.cr.execute(request, params)
            sr_no = 1
            total_amount = 0
            partner_dict = {}
            for row_data in self.env.cr.dictfetchall():
                partner_id = row_data.get('partner_id',False)
                if row_data.get('credit') > 0:  
                    if partner_id and not partner_dict.get(partner_id):
                        partner_dict.update({partner_id:[{'ref':row_data.get('ref'),
                                                                          'credit':row_data.get('credit'),
                                                                          'date':row_data.get('date'),
                                                                          'move_id':row_data.get('move_id'),
                                                                          }]})
                    else:
                        partner_dict.get(partner_id).append({'ref':row_data.get('ref'),
                                                                          'credit':row_data.get('credit'),
                                                                          'date':row_data.get('date'),
                                                                          'move_id':row_data.get('move_id'),
                                                                          })
                        
            for data in partner_dict:
                partner_data = partner_dict.get(data)
                total_partner_amount = 0
                if partner_data: 
                    col = 0
                    partner_id = self.env['res.partner'].browse(data)
                    worksheet.write(row,col,sr_no)
                    col += 1
                    worksheet.write(row,col,partner_id.name)
                    col += 1
                    pan_no = ''
                    if partner_id.pan_no:
                        pan_no = partner_id.pan_no
                    worksheet.write(row,col,pan_no)
                    ref_col = col+1
                    
                    date_col = col+2
                    credit_col = col+3
                    total_col = col+4
                    for dict_data in partner_data:
                        reference =dict_data.get('ref')
                        if not reference and dict_data.get('move_id'):
                            account_move_id = self.env['account.move'].browse(dict_data.get('move_id'))
                            reference = account_move_id.name
                        
                        worksheet.write(row,ref_col,reference)
                        col += 1
                        create_date =  datetime.strptime(dict_data.get('date'),'%Y-%m-%d').strftime('%d-%m-%Y')
                        worksheet.write(row,date_col,create_date)
                        col += 1
                        worksheet.write(row,credit_col,dict_data.get('credit'))
                        total_amount += dict_data.get('credit')
                        total_partner_amount+=dict_data.get('credit')
                        col += 1
                        row += 1
                    
                    worksheet.write(row-1,total_col,total_partner_amount,style1_sub_total)
                    sr_no += 1
                    
            if total_amount:
                worksheet.write(row,4, "Total",style1_total)
                worksheet.write(row,5,total_amount,style1_total)
                worksheet.write(row,6,total_amount,style1_total)
                row+=1
            
        fp = StringIO()
        workbook.save(fp)
        export_id = self.env['tds.excel'].create({'excel_file': base64.encodestring(fp.getvalue()), 'file_name': filename})
        fp.close()     

        return {
                'view_mode': 'form',
                'res_id': export_id.id,
                'res_model': 'tds.excel',
                'view_type': 'form',
                'type': 'ir.actions.act_window',
                'target': 'new',
                } 

class tds_excel(models.TransientModel):
    _name= "tds.excel"
    
    excel_file =  fields.Binary('TDS Report')
    file_name =  fields.Char('Excel File', size=64)
    