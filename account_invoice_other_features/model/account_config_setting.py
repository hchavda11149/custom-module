from odoo import models,fields,api,_
from odoo.tools import amount_to_text_en

class ExtraChargeSetting(models.TransientModel):
    _inherit = 'account.config.settings'

    packaging_cost = fields.Boolean(string='Allow Packing Cost on invoice', help="Allow Packing Cost On Invoice")
    packaging_cost_account = fields.Many2one('account.account', string='Packaging Cost Account')
    
    round_off = fields.Boolean(string='Allow rounding of invoice amount', help="Allow rounding of invoice amount")
    round_off_account = fields.Many2one('account.account', string='Round Off Account')
    
    discount_amount = fields.Boolean(string='Allow Discount on invoice', help="Allow Discount On Invoice")
    discount_amount_account = fields.Many2one('account.account', string='Discount Account')

    tds_customer_bill = fields.Boolean('TDS On Customer Bill')
    tds_vendor_bill = fields.Boolean('TDS On Vendor Bill')
    tcs_allow_bill = fields.Boolean('TCS Allow Bill')
    
    @api.multi
    def set_round_off(self):
        ir_values_obj = self.env['ir.values']
        ir_values_obj.sudo().set_default('account.config.settings', "round_off", self.round_off)
        ir_values_obj.sudo().set_default('account.config.settings', "round_off_account", self.round_off_account.id)
    
    @api.multi
    def set_packaging_cost(self):
        ir_values_obj = self.env['ir.values']
        ir_values_obj.sudo().set_default('account.config.settings', "packaging_cost", self.packaging_cost)
        ir_values_obj.sudo().set_default('account.config.settings', "packaging_cost_account", self.packaging_cost_account.id)
    
    @api.multi
    def set_discount_amount(self):
        ir_values_obj = self.env['ir.values']
        ir_values_obj.sudo().set_default('account.config.settings', "discount_amount", self.discount_amount)
        ir_values_obj.sudo().set_default('account.config.settings', "discount_amount_account", self.discount_amount_account.id)

    @api.multi
    def set_tds(self):
        ir_values_obj = self.env['ir.values']
        ir_values_obj.sudo().set_default('account.config.settings', "tds_customer_bill", self.tds_customer_bill)
        ir_values_obj.sudo().set_default('account.config.settings', "tds_vendor_bill", self.tds_vendor_bill)
        ir_values_obj.sudo().set_default('account.config.settings', "tcs_allow_bill", self.tcs_allow_bill)

        
