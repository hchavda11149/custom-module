from odoo import models,fields,api,_
from odoo.exceptions import UserError, RedirectWarning, ValidationError
from odoo.tools import float_is_zero

class AccountInvoice(models.Model):
    
    _inherit = 'account.invoice'

        
    packaging_cost = fields.Float('Packaging & Forwarding Cost')
    is_packaging_cost = fields.Boolean(compute='get_is_packaging_cost')

    discount_amount = fields.Float('Discount')
    is_discount_amount = fields.Boolean(compute='get_discount_cost')
    
    round_off_value = fields.Float(compute='_compute_amount', string='Round off amount')
    rounded_total = fields.Float(compute='_compute_amount', string='Rounded Total')
    round_active = fields.Boolean(compute='get_round_active')
    
    tds_customer_bill = fields.Boolean(compute='get_is_tds')
    tds_vendor_bill = fields.Boolean(compute='get_is_tds')
    tds_tax_id = fields.Many2one('account.tax', string="TDS Tax")
    tds_amount = fields.Float(compute='cal_tds_amount', string="TDS Amount")

    tcs_allow_bill = fields.Boolean(compute='get_is_tcs')
    tcs_tax_id = fields.Many2one('account.tax', string="TCS Tax")
    tcs_amount = fields.Float(compute='cal_tcs_amount', string="TCS Amount")

    
    @api.multi
    @api.depends('tds_tax_id','amount_untaxed')
    def cal_tds_amount(self):
        for record in self:
            if record.tds_tax_id and record.amount_untaxed:
                dic_taxes = record.tds_tax_id.compute_all(record.amount_untaxed, record.currency_id, 1, False, record.partner_id)['taxes']
                tds_amount = 0
                for tax in dic_taxes:
                    tds_amount += tax['amount']
                record.tds_amount = round(-tds_amount)
            else:
                record.tds_amount = 0.0

    @api.multi
    @api.depends('tcs_tax_id', 'amount_untaxed', 'amount_tax')
    def cal_tcs_amount(self):
        for record in self:
            if record.tcs_tax_id and record.amount_untaxed:
                dic_taxes = \
                record.tcs_tax_id.compute_all(record.amount_untaxed + record.amount_tax, record.currency_id, 1, False, record.partner_id)[
                    'taxes']
                tcs_amount = 0
                for tax in dic_taxes:
                    tcs_amount += tax['amount']
                record.tcs_amount = tcs_amount
            else:
                record.tcs_amount = 0.0

    def get_is_tds(self):
        ir_values = self.env['ir.values']
        for i in self:
            i.tds_customer_bill = ir_values.get_default('account.config.settings', 'tds_customer_bill')
            i.tds_vendor_bill = ir_values.get_default('account.config.settings', 'tds_vendor_bill')


    def get_is_tcs(self):
        ir_values = self.env['ir.values']
        for i in self:
            i.tcs_allow_bill = ir_values.get_default('account.config.settings', 'tcs_allow_bill')

    
    def get_round_active(self):
        ir_values = self.env['ir.values']
        for i in self:
            i.round_active = ir_values.get_default('account.config.settings', 'round_off')
    
    def get_is_packaging_cost(self):
        ir_values = self.env['ir.values']
        for i in self:
            i.is_packaging_cost = ir_values.get_default('account.config.settings', 'packaging_cost')
            
    def get_discount_cost(self):
        ir_values = self.env['ir.values']
        for i in self:
            i.is_discount_amount = ir_values.get_default('account.config.settings', 'discount_amount')

    
    @api.model
    def default_get(self,fields):
        res = super(AccountInvoice,self).default_get(fields)
        ir_values = self.env['ir.values']
        is_packaging_cost = ir_values.get_default('account.config.settings', 'packaging_cost')
        is_discount_amount = ir_values.get_default('account.config.settings', 'discount_amount')
        tds_customer_bill = ir_values.get_default('account.config.settings', 'tds_customer_bill')
        tds_vendor_bill = ir_values.get_default('account.config.settings', 'tds_vendor_bill')
        tcs_allow_bill = ir_values.get_default('account.config.settings', 'tcs_allow_bill')
        res.update({'is_packaging_cost':is_packaging_cost,
                    'is_discount_amount':is_discount_amount,
                    'tds_customer_bill':tds_customer_bill,
                    'tds_vendor_bill':tds_vendor_bill,
                    'tcs_allow_bill': tcs_allow_bill,
                    })
        
        return res
    
    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id',
                 'date_invoice', 'type','packaging_cost','discount_amount','tds_amount', 'tcs_amount')
    def _compute_amount(self):
        untaxed_amount = sum(line.price_subtotal for line in self.invoice_line_ids)
        self.amount_untaxed = untaxed_amount + self.packaging_cost-self.discount_amount  
        self.amount_tax = sum(line.amount for line in self.tax_line_ids)
        self.rounded_total = round(self.amount_untaxed + self.amount_tax + self.tds_amount + self.tcs_amount)
        self.amount_total = self.amount_untaxed + self.amount_tax + self.tds_amount + self.tcs_amount
        self.round_off_value = self.rounded_total - (self.amount_untaxed + self.amount_tax+self.tds_amount + self.tcs_amount)
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign

    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line_ids.price_subtotal',
        'move_id.line_ids.amount_residual',
        'move_id.line_ids.currency_id')
    def _compute_residual(self):
        residual = 0.0
        residual_company_signed = 0.0
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        for line in self.sudo().move_id.line_ids:
            if line.account_id.internal_type in ('receivable', 'payable'):
                residual_company_signed += line.amount_residual
                if line.currency_id == self.currency_id:
                    residual += line.amount_residual_currency if line.currency_id else line.amount_residual
                else:
                    from_currency = (line.currency_id and line.currency_id.with_context(
                        date=line.date)) or line.company_id.currency_id.with_context(date=line.date)
                    residual += from_currency.compute(line.amount_residual, self.currency_id)
        self.residual_company_signed = abs(residual_company_signed) * sign
        self.residual_signed = abs(residual) * sign
        if self.round_active is True and self.type == 'out_invoice':
            self.residual = round(abs(residual))
        elif self.round_active is True and self.type == 'in_invoice':
            self.residual = round(abs(residual))
        else:
            self.residual = abs(residual)
        digits_rounding_precision = self.currency_id.rounding
        if float_is_zero(self.residual, precision_rounding=digits_rounding_precision):
            self.reconciled = True
        else:
            self.reconciled = False
    
    @api.multi
    def get_taxes_values(self):
        tax_grouped = super(AccountInvoice,self).get_taxes_values()

        if self.discount_amount or self.packaging_cost:
            total_discount = self.packaging_cost - self.discount_amount
            total_invoice_amount = self.amount_untaxed - self.packaging_cost +  self.discount_amount
              
            for line in self.invoice_line_ids:
                line_discount = (total_discount/total_invoice_amount)*line.price_subtotal
                if not line.quantity == 0:
                    line_discount = line_discount/line.quantity
    
                taxes = line.invoice_line_tax_ids.compute_all(line_discount, self.currency_id, line.quantity, line.product_id, self.partner_id)['taxes']
    
                for tax in taxes:
                    val = self._prepare_tax_line_vals(line, tax)
                    key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)
    
                    if key not in tax_grouped:
                        tax_grouped[key] = val
                    else:
                        tax_grouped[key]['amount'] += val['amount']
                        tax_grouped[key]['base'] += val['base']
                                        
#         if self.invoice_line_ids:
#             other_charge_tax = self.env['account.tax']
#             for line in self.invoice_line_ids:
#                 for tax_line in line.invoice_line_tax_ids:
#                     other_charge_tax +=tax_line 
#             
#             if other_charge_tax:
#                 other_charge_tax = other_charge_tax.mapped('id')
#                 other_charge_tax=list(set(other_charge_tax))    
#                 other_charge_tax = self.env['account.tax'].browse(other_charge_tax)
#                 
#                 other_charge = self.packaging_cost - self.discount_amount
#                 dic_taxes = other_charge_tax.compute_all(other_charge, self.currency_id, 1, False, self.partner_id)['taxes']
#                 for tax in dic_taxes:
#                     val = self._prepare_tax_line_vals(self.invoice_line_ids[0], tax)
#                     key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)
#      
#                     if key not in tax_grouped:
#                         tax_grouped[key] = val
#                     else:
#                         tax_grouped[key]['amount'] += val['amount']
#                         tax_grouped[key]['base'] += val['base']
        return tax_grouped
    
    @api.onchange('invoice_line_ids','packaging_cost','discount_amount', 'tds_amount', 'tcs_amount')
    def _onchange_invoice_line_ids(self):
        return super(AccountInvoice,self)._onchange_invoice_line_ids()

    @api.multi
    def action_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        account_move = self.env['account.move']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise UserError(_('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line_ids:
                raise UserError(_('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            if not inv.date_invoice:
                inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
            company_currency = inv.company_id.currency_id

            # create move lines (one per invoice line + eventual taxes and analytic lines)
            iml = inv.invoice_line_move_line_get()
            iml += inv.tax_line_move_line_get()

            diff_currency = inv.currency_id != company_currency
            # create one move line for the total and possibly adjust the other lines amount
            total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, iml)
            
            # set the total amount with other charge
            if self.type == 'out_invoice':
                total = self.amount_total
            if self.type == 'in_invoice':
                total = -self.amount_total
                    
            name = inv.name or '/'
            if inv.payment_term_id:
                totlines = inv.with_context(ctx).payment_term_id.with_context(currency_id=company_currency.id).compute(total, inv.date_invoice)[0]
                res_amount_currency = total_currency
                ctx['date'] = inv._get_currency_rate_date()
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency
                    if self.type == 'out_invoice':
                        price = t[1]
                        ir_values = self.env['ir.values']
                        if self.is_packaging_cost is True and self.packaging_cost:
                            acc_id = ir_values.get_default('account.config.settings', 'packaging_cost_account')
                            iml.append({
                                'type': 'dest',
                                'name': "Packaging & Forwarding Cost",
                                'price': - self.packaging_cost,
                                'account_id': acc_id,
                                'date_maturity': t[0],
                                'amount_currency': diff_currency and amount_currency,
                                'currency_id': diff_currency and inv.currency_id.id,
                                'invoice_id': inv.id
                            })
                        if self.is_discount_amount is True and self.discount_amount:
                            acc_id = ir_values.get_default('account.config.settings', 'discount_amount_account')
                            iml.append({
                                'type': 'dest',
                                'name': "Discount",
                                'price': self.discount_amount,
                                'account_id': acc_id,
                                'date_maturity': t[0],
                                'amount_currency': diff_currency and amount_currency,
                                'currency_id': diff_currency and inv.currency_id.id,
                                'invoice_id': inv.id
                            })
                        if self.round_active is True and self.round_off_value != 0:
                            ir_values = self.env['ir.values']
                            acc_id = ir_values.get_default('account.config.settings', 'round_off_account')
                            price = price+self.round_off_value
                            iml.append({
                                'type': 'dest',
                                'name': "Round off",
                                'price': -self.round_off_value,
                                'account_id': acc_id,
                                'date_maturity': t[0],
                                'amount_currency': diff_currency and amount_currency,
                                'currency_id': diff_currency and inv.currency_id.id,
                                'invoice_id': inv.id
                            })
                        if self.tds_customer_bill is True and self.tds_tax_id and self.tds_amount:
                            iml.append({
                                'type': 'dest',
                                'name': self.tds_tax_id.name,
                                'price': -self.tds_amount,
                                'account_id': self.tds_tax_id.account_id.id,
                                'date_maturity': t[0],
                                'amount_currency': diff_currency and amount_currency,
                                'currency_id': diff_currency and inv.currency_id.id,
                                'invoice_id': inv.id
                            })

                        if self.tcs_allow_bill is True and self.tcs_tax_id and self.tcs_amount:
                            iml.append({
                                'type': 'dest',
                                'name': self.tcs_tax_id.name,
                                'price': -self.tcs_amount,
                                'account_id': self.tcs_tax_id.account_id.id,
                                'date_maturity': t[0],
                                'amount_currency': diff_currency and amount_currency,
                                'currency_id': diff_currency and inv.currency_id.id,
                                'invoice_id': inv.id
                            })

                        iml.append({
                            'type': 'dest',
                            'name': name,
                            'price': price,
                            'account_id': inv.account_id.id,
                            'date_maturity': t[0],
                            'amount_currency': diff_currency and amount_currency,
                            'currency_id': diff_currency and inv.currency_id.id,
                            'invoice_id': inv.id
                        })
                    elif self.type == 'in_invoice':
                        price = t[1]
                        ir_values = self.env['ir.values']
                        if self.is_packaging_cost is True and self.packaging_cost:
                            acc_id = ir_values.get_default('account.config.settings', 'packaging_cost_account')
                            iml.append({
                                'type': 'dest',
                                'name': "Packaging & Forwarding Cost",
                                'price': self.packaging_cost,
                                'account_id': acc_id,
                                'date_maturity': t[0],
                                'amount_currency': diff_currency and amount_currency,
                                'currency_id': diff_currency and inv.currency_id.id,
                                'invoice_id': inv.id
                            })

                        if self.round_active is True and self.round_off_value != 0:
                            ir_values = self.env['ir.values']
                            acc_id = ir_values.get_default('account.config.settings', 'round_off_account')
                            price = price-self.round_off_value
                            iml.append({
                                'type': 'dest',
                                'name': "Round off",
                                'price': self.round_off_value,
                                'account_id': acc_id,
                                'date_maturity': t[0],
                                'amount_currency': diff_currency and amount_currency,
                                'currency_id': diff_currency and inv.currency_id.id,
                                'invoice_id': inv.id
                            })

                        if self.is_discount_amount is True and self.discount_amount:
                            acc_id = ir_values.get_default('account.config.settings', 'discount_amount_account')
                            iml.append({
                                'type': 'dest',
                                'name': "Discount",
                                'price': -self.discount_amount,
                                'account_id': acc_id,
                                'date_maturity': t[0],
                                'amount_currency': diff_currency and amount_currency,
                                'currency_id': diff_currency and inv.currency_id.id,
                                'invoice_id': inv.id
                            })
                        if self.tds_vendor_bill is True and self.tds_tax_id and self.tds_amount:
                            iml.append({
                                'type': 'dest',
                                'name': self.tds_tax_id.name,
                                'price': self.tds_amount,
                                'account_id': self.tds_tax_id.account_id.id,
                                'date_maturity': t[0],
                                'amount_currency': diff_currency and amount_currency,
                                'currency_id': diff_currency and inv.currency_id.id,
                                'invoice_id': inv.id
                            })

     
                        iml.append({
                            'type': 'dest',
                            'name': name,
                            'price': price,
                            'account_id': inv.account_id.id,
                            'date_maturity': t[0],
                            'amount_currency': diff_currency and amount_currency,
                            'currency_id': diff_currency and inv.currency_id.id,
                            'invoice_id': inv.id
                        })

                    else:            
                        iml.append({
                            'type': 'dest',
                            'name': name,
                            'price': t[1],
                            'account_id': inv.account_id.id,
                            'date_maturity': t[0],
                            'amount_currency': diff_currency and amount_currency,
                            'currency_id': diff_currency and inv.currency_id.id,
                            'invoice_id': inv.id
                        })
            else:
                if self.type == 'out_invoice':
                    ir_values = self.env['ir.values']
                    
                    if self.is_packaging_cost is True and self.packaging_cost:
                        acc_id = ir_values.get_default('account.config.settings', 'packaging_cost_account')
                        iml.append({
                            'type': 'dest',
                            'name': "Packaging & Forwarding Cost",
                            'price': -self.packaging_cost,
                            'account_id': acc_id,
                            'date_maturity': inv.date_due,
                            'amount_currency': diff_currency and amount_currency,
                            'currency_id': diff_currency and inv.currency_id.id,
                            'invoice_id': inv.id
                        })
                    if self.is_discount_amount is True and self.discount_amount:
                        acc_id = ir_values.get_default('account.config.settings', 'discount_amount_account')
                        iml.append({
                            'type': 'dest',
                            'name': "Discount",
                            'price': self.discount_amount,
                            'account_id': acc_id,
                            'date_maturity': inv.date_due,
                            'amount_currency': diff_currency and amount_currency,
                            'currency_id': diff_currency and inv.currency_id.id,
                            'invoice_id': inv.id
                        })
                    if self.round_active is True and self.round_off_value != 0:
                        total=total+self.round_off_value
                        ir_values = self.env['ir.values']
                        acc_id = ir_values.get_default('account.config.settings', 'round_off_account')
                        iml.append({
                            'type': 'dest',
                            'name': "Round off",
                            'price': -self.round_off_value,
                            'account_id': acc_id,
                            'date_maturity': inv.date_due,
                            'amount_currency': diff_currency and total_currency,
                            'currency_id': diff_currency and inv.currency_id.id,
                            'invoice_id': inv.id
                        })
                    if self.tds_customer_bill is True and self.tds_tax_id and self.tds_amount:
                        iml.append({
                            'type': 'dest',
                            'name': self.tds_tax_id.name,
                            'price': -self.tds_amount,
                            'account_id': self.tds_tax_id.account_id.id,
                            'date_maturity': inv.date_due,
                            'amount_currency': diff_currency and total_currency,
                            'currency_id': diff_currency and inv.currency_id.id,
                            'invoice_id': inv.id
                        })

                    if self.tcs_allow_bill is True and self.tcs_tax_id and self.tcs_amount:
                        iml.append({
                            'type': 'dest',
                            'name': self.tcs_tax_id.name,
                            'price': -self.tcs_amount,
                            'account_id': self.tcs_tax_id.account_id.id,
                            'date_maturity': inv.date_due,
                            'amount_currency': diff_currency and total_currency,
                            'currency_id': diff_currency and inv.currency_id.id,
                            'invoice_id': inv.id
                        })
                        
                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': total,
                        'account_id': inv.account_id.id,
                        'date_maturity': inv.date_due,
                        'amount_currency': diff_currency and total_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
                elif self.type == 'in_invoice':
                    
                    ir_values = self.env['ir.values']
                    if self.is_packaging_cost is True and self.packaging_cost:
                        acc_id = ir_values.get_default('account.config.settings', 'packaging_cost_account')
                        iml.append({
                            'type': 'dest',
                            'name': "Packaging & Forwarding Cost",
                            'price': self.packaging_cost,
                            'account_id': acc_id,
                            'date_maturity': inv.date_due,
                            'amount_currency': diff_currency and amount_currency,
                            'currency_id': diff_currency and inv.currency_id.id,
                            'invoice_id': inv.id
                        })
                    if self.is_discount_amount is True and self.discount_amount:
                        acc_id = ir_values.get_default('account.config.settings', 'discount_amount_account')
                        iml.append({
                            'type': 'dest',
                            'name': "Discount",
                            'price': -self.discount_amount,
                            'account_id': acc_id,
                            'date_maturity': inv.date_due,
                            'amount_currency': diff_currency and amount_currency,
                            'currency_id': diff_currency and inv.currency_id.id,
                            'invoice_id': inv.id
                        })
                    if self.round_active is True and self.round_off_value != 0:
                        total=total-self.round_off_value
                        ir_values = self.env['ir.values']
                        acc_id = ir_values.get_default('account.config.settings', 'round_off_account')
                        iml.append({
                            'type': 'dest',
                            'name': "Round off",
                            'price': self.round_off_value,
                            'account_id': acc_id,
                            'date_maturity': inv.date_due,
                            'amount_currency': diff_currency and total_currency,
                            'currency_id': diff_currency and inv.currency_id.id,
                            'invoice_id': inv.id
                        })
                    if self.tds_vendor_bill is True and self.tds_tax_id and self.tds_amount:

                        iml.append({
                            'type': 'dest',
                            'name': self.tds_tax_id.name,
                            'price': self.tds_amount,
                            'account_id': self.tds_tax_id.account_id.id,
                            'date_maturity': inv.date_due,
                            'amount_currency': diff_currency and total_currency,
                            'currency_id': diff_currency and inv.currency_id.id,
                            'invoice_id': inv.id
                        })

                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': total,
                        'account_id': inv.account_id.id,
                        'date_maturity': inv.date_due,
                        'amount_currency': diff_currency and total_currency,    
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })

                else:        
                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': total,
                        'account_id': inv.account_id.id,
                        'date_maturity': inv.date_due,
                        'amount_currency': diff_currency and total_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
            part = self.env['res.partner']._find_accounting_partner(inv.partner_id)
            line = [(0, 0, self.line_get_convert(l, part.id)) for l in iml]
            line = inv.group_lines(iml, line)

            journal = inv.journal_id.with_context(ctx)
            line = inv.finalize_invoice_move_lines(line)

            date = inv.date or inv.date_invoice
            move_vals = {
                'ref': inv.reference,
                'line_ids': line,
                'journal_id': journal.id,
                'date': date,
                'narration': inv.comment,
            }
            ctx['company_id'] = inv.company_id.id
            ctx['invoice'] = inv
            ctx_nolang = ctx.copy()
            ctx_nolang.pop('lang', None)
            move = account_move.with_context(ctx_nolang).create(move_vals)
            # Pass invoice in context in method post: used if you want to get the same
            # account move reference when creating the same invoice after a cancelled one:
            move.post()
            # make the invoice point to that move
            vals = {
                'move_id': move.id,
                'date': date,
                'move_name': move.name,
            }
            inv.with_context(ctx).write(vals)
        return True

    @api.multi
    def cal_custom(self):
        invoice_ids = self.env['account.invoice'].search([])
        for inv in invoice_ids:
            for line in inv.invoice_line_ids:
                line._compute_price()


class AccountInvoiceLine(models.Model):   
    _inherit = 'account.invoice.line'

    line_discount_value = fields.Float(compute='cal_line_wise_discount',string='Line Discount')
    
    @api.multi
    @api.depends('invoice_id.discount_amount','invoice_id.packaging_cost')
    def cal_line_wise_discount(self):
        for line in self:
            line_discount = 0
            if line.invoice_id.packaging_cost or line.invoice_id.discount_amount:
                total_invoice_amount = line.invoice_id.amount_untaxed - line.invoice_id.packaging_cost +  line.invoice_id.discount_amount
                total_discount = line.invoice_id.discount_amount
                if total_invoice_amount != 0:
                    currency = line.invoice_id and line.invoice_id.currency_id or None
                    price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                    taxes = False
                    if line.invoice_line_tax_ids:
                        taxes = line.invoice_line_tax_ids.compute_all(price, currency, line.quantity, product=line.product_id, partner=line.invoice_id.partner_id)
                    price_subtotal = price_subtotal_signed = taxes['total_excluded'] if taxes else line.quantity * price
                      
                    line_discount = (total_discount/total_invoice_amount)*price_subtotal
    
            line.line_discount_value = line_discount
                
#     line_discount_value = fields.Float(compute='cal_line_wise_discount',string='Line Discount',store=True)
#     line_packaging_cost = fields.Float(compute='cal_line_wise_discount',string='Line Packaging Cost',store=True)	
# 
#     @api.multi
#     @api.depends('price_unit', 'discount','quantity','invoice_id.discount_amount','invoice_id.packaging_cost')
#     def cal_line_wise_discount(self):
#         for line in self:
#             #for line in inv_line.invoice_id.invoice_line_ids:
#                 line_discount = 0
#                 line_packaging = 0
#                 if line.invoice_id.discount_amount:
#                     total_invoice_amount_discount = line.invoice_id.amount_untaxed + line.invoice_id.discount_amount - line.invoice_id.packaging_cost
#                     if total_invoice_amount_discount > 0:
#                         line_discount = (line.invoice_id.discount_amount/total_invoice_amount_discount)*line.price_subtotal
#                         if not line.quantity == 0:
#                             line_discount = line_discount/line.quantity                    
#                           
#                 line.line_discount_value = line_discount
# 
#                 if line.invoice_id.packaging_cost:
#                     total_invoice_amount_discount = line.invoice_id.amount_untaxed + line.invoice_id.discount_amount - line.invoice_id.packaging_cost
#                     if total_invoice_amount_discount > 0:
#                         line_packaging = (line.invoice_id.packaging_cost/total_invoice_amount_discount)*line.price_subtotal
#                         if not line.quantity == 0:
#                             line_packaging = line_packaging/line.quantity                    
#                           
#                 line.line_packaging_cost = line_packaging
                   
#                 if line.invoice_id.packaging_cost:
#                     total_invoice_amount_package = line.invoice_id.amount_untaxed - line.invoice_id.packaging_cost
#                       
#                     line_packaging = (line.invoice_id.packaging_cost/total_invoice_amount_package)*line.price_subtotal
#                 line.line_packaging_cost = line_packaging
#                 
#                 if line.invoice_id.discount_amount and line.invoice_id.packaging_cost:
#                     if line.invoice_id.discount_amount > line.invoice_id.packaging_cost:
#                         discount_amount = line.invoice_id.amount_untaxed - line.invoice_id.packaging_cost
#                         packing_amount = line.invoice_id.amount_untaxed + line.invoice_id.discount_amount
#                         
#                         total_invoice_amount_discount = discount_amount + line.invoice_id.discount_amount
#                         line_discount = (line.invoice_id.discount_amount/total_invoice_amount_discount)*line.price_subtotal
#                          
#                         total_invoice_amount_package = packing_amount - line.invoice_id.packaging_cost
#                         line_packaging = (line.invoice_id.packaging_cost/total_invoice_amount_package)*line.price_subtotal
#                     
#                     
#                     elif line.invoice_id.discount_amount < line.invoice_id.packaging_cost:
#                        
#                         diff = line.invoice_id.packaging_cost - line.invoice_id.discount_amount
#                         
#                         discount_amount = line.invoice_id.amount_untaxed - line.invoice_id.packaging_cost
#                         
#                         packing_amount = line.invoice_id.amount_untaxed + line.invoice_id.discount_amount
#                        
#                         total_invoice_amount_discount = discount_amount + line.invoice_id.discount_amount
#                         line_discount = (line.invoice_id.discount_amount/total_invoice_amount_discount)*line.price_subtotal
#                          
#                         total_invoice_amount_package = packing_amount - line.invoice_id.packaging_cost
#                         line_packaging = (line.invoice_id.packaging_cost/total_invoice_amount_package)*line.price_subtotal
#                     
#                     if line.invoice_id.discount_amount == line.invoice_id.packaging_cost:
#                         discount_amount = line.invoice_id.amount_untaxed - line.invoice_id.packaging_cost
#                         packing_amount = line.invoice_id.amount_untaxed + line.invoice_id.discount_amount
#                         
#                         total_invoice_amount_discount = line.invoice_id.discount_amount + discount_amount
#                         try:
#                             line_discount = (line.invoice_id.discount_amount/total_invoice_amount_discount)*line.price_subtotal
#                         except ZeroDivisionError:
#                             print"",111
#                         total_invoice_amount_package = packing_amount - line.invoice_id.packaging_cost
#                         try:
#                             line_packaging = (line.invoice_id.packaging_cost/total_invoice_amount_package)*line.price_subtotal
#                         except ZeroDivisionError:
#                             print"",111
#                         
#                 line.line_discount_value = line_discount
#                 line.line_packaging_cost = line_packaging
    
#     @api.multi
#     @api.depends('price_unit', 'discount','quantity','invoice_id.discount_amount','invoice_id.packaging_cost')
#     def cal_line_wise_discount(self):
#         for inv_line in self:
#             for line in inv_line.invoice_id.invoice_line_ids:
#                 line_discount = 0
#                 line_packaging = 0
#                 if line.invoice_id.discount_amount:
#                     total_invoice_amount_discount = line.invoice_id.amount_untaxed + line.invoice_id.discount_amount
#                     line_discount = (line.invoice_id.discount_amount/total_invoice_amount_discount)*line.price_subtotal
#                 line.line_discount_value = line_discount
#                    
#                 if line.invoice_id.packaging_cost:
#                     total_invoice_amount_package = line.invoice_id.amount_untaxed - line.invoice_id.packaging_cost
#                       
#                     line_packaging = (line.invoice_id.packaging_cost/total_invoice_amount_package)*line.price_subtotal
#                 line.line_packaging_cost = line_packaging
#                 
#                 if line.invoice_id.discount_amount and line.invoice_id.packaging_cost:
#                     if line.invoice_id.discount_amount > line.invoice_id.packaging_cost:
#                         discount_amount = line.invoice_id.amount_untaxed - line.invoice_id.packaging_cost
#                         packing_amount = line.invoice_id.amount_untaxed + line.invoice_id.discount_amount
#                         
#                         total_invoice_amount_discount = discount_amount + line.invoice_id.discount_amount
#                         line_discount = (line.invoice_id.discount_amount/total_invoice_amount_discount)*line.price_subtotal
#                          
#                         total_invoice_amount_package = packing_amount - line.invoice_id.packaging_cost
#                         line_packaging = (line.invoice_id.packaging_cost/total_invoice_amount_package)*line.price_subtotal
#                     
#                     
#                     elif line.invoice_id.discount_amount < line.invoice_id.packaging_cost:
#                        
#                         diff = line.invoice_id.packaging_cost - line.invoice_id.discount_amount
#                         
#                         discount_amount = line.invoice_id.amount_untaxed - line.invoice_id.packaging_cost
#                         
#                         packing_amount = line.invoice_id.amount_untaxed + line.invoice_id.discount_amount
#                        
#                         total_invoice_amount_discount = discount_amount + line.invoice_id.discount_amount
#                         line_discount = (line.invoice_id.discount_amount/total_invoice_amount_discount)*line.price_subtotal
#                          
#                         total_invoice_amount_package = packing_amount - line.invoice_id.packaging_cost
#                         line_packaging = (line.invoice_id.packaging_cost/total_invoice_amount_package)*line.price_subtotal
#                     
#                     if line.invoice_id.discount_amount == line.invoice_id.packaging_cost:
#                         discount_amount = line.invoice_id.amount_untaxed - line.invoice_id.packaging_cost
#                         packing_amount = line.invoice_id.amount_untaxed + line.invoice_id.discount_amount
#                         
#                         total_invoice_amount_discount = line.invoice_id.discount_amount + discount_amount
#                         try:
#                             line_discount = (line.invoice_id.discount_amount/total_invoice_amount_discount)*line.price_subtotal
#                         except ZeroDivisionError:
#                             print"",111
#                         total_invoice_amount_package = packing_amount - line.invoice_id.packaging_cost
#                         try:
#                             line_packaging = (line.invoice_id.packaging_cost/total_invoice_amount_package)*line.price_subtotal
#                         except ZeroDivisionError:
#                             print"",111
#                         
#                 line.line_discount_value = line_discount
#                 line.line_packaging_cost = line_packaging
                

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity',
        'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.company_id',
        'invoice_id.date_invoice', 'invoice_id.date','invoice_id.discount_amount','invoice_id.packaging_cost')    
    def _compute_price(self):
        res = super(AccountInvoiceLine,self)._compute_price()
        self.with_context(call_from_current_method=True).cal_line_wise_discount()
        self.price_subtotal_signed = self.price_subtotal_signed - self.line_discount_value
        return res
