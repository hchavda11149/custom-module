from odoo import models,fields,api,_

class ResPartner(models.Model):
    
    _inherit = 'res.partner'
    
    is_apply_withholding = fields.Boolean('Apply TDS/Withholding if the turnover crosses the threshold',copy=False,default=False)
    withholding_amount = fields.Float('Withholding Amount',copy=False,default=0.0)
    
    