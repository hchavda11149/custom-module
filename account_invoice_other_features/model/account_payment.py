from odoo import models,fields,api,_
from odoo.exceptions import UserError, RedirectWarning, ValidationError
from odoo.tools import float_is_zero
from datetime import datetime

class account_abstract_payment(models.AbstractModel):
    _inherit = "account.abstract.payment"

    tds_customer_payment = fields.Boolean(compute='get_is_tds')
    tds_vendor_payment = fields.Boolean(compute='get_is_tds')
    tds_tax_id = fields.Many2one('account.tax',string="TDS Tax")
    tds_amount = fields.Float(compute='cal_tds_amount',string="TDS Amount")
    paid_amount = fields.Float(compute='cal_tds_amount',string="Amount")
    
    def get_is_tds(self):
        ir_values = self.env['ir.values']
        for i in self:
            i.tds_customer_payment = ir_values.get_default('account.config.settings', 'tds_customer_bill')
            i.tds_vendor_payment = ir_values.get_default('account.config.settings', 'tds_vendor_bill')

    @api.multi
    @api.depends('tds_tax_id','amount')
    def cal_tds_amount(self):
        for record in self:
            if record.tds_tax_id and record.amount and record.partner_id:
                amount = record.amount
#                 if self.invoice_ids:
#                     amount = self.invoice_ids[0].amount_untaxed
                dic_taxes = record.tds_tax_id.compute_all(amount, record.currency_id, 1, False, record.partner_id)['taxes']
                tds_amount = 0
                for tax in dic_taxes:
                    tds_amount += tax['amount']
                record.tds_amount = round(tds_amount)
                record.paid_amount = record.amount - record.tds_amount
            else:
                record.tds_amount = 0.0
                record.paid_amount = record.amount - record.tds_amount

    @api.model
    def default_get(self,fields):
        res = super(account_abstract_payment,self).default_get(fields)
        ir_values = self.env['ir.values']
        tds_customer_payment = ir_values.get_default('account.config.settings', 'tds_customer_bill')
        tds_vendor_payment = ir_values.get_default('account.config.settings', 'tds_vendor_bill')
        res.update({
                    'tds_customer_payment':tds_customer_payment,
                    'tds_vendor_payment':tds_vendor_payment,
                    })
        
        return res

    @api.onchange('payment_type','journal_id')
    def onchange_payment_type(self):
        
        if self.payment_type:
            self.tds_tax_id = False
            if self.payment_type == 'outbound' and self.tds_vendor_payment:
                return {'domain': {'tds_tax_id': [('is_tds', '=',True),('type_tax_use','=','purchase'),('company_id','=',self.company_id.id)]}}
            elif self.payment_type == 'inbound' and self.tds_customer_payment:
                return {'domain': {'tds_tax_id': [('is_tds', '=',True),('type_tax_use','=','sale'),('company_id','=',self.company_id.id)]}}
        else:
            return {'domain': {'tds_tax_id': [('id', '=', False)]}}

class AccountPayment(models.Model):
    
    _inherit = 'account.payment'
    
    tds_amount = fields.Float(compute='cal_tds_amount',string="TDS Amount")
    paid_amount = fields.Float(compute='cal_tds_amount',string="Amount")
    
    def get_is_tds(self):
        ir_values = self.env['ir.values']
        for i in self:
            i.tds_customer_payment = ir_values.get_default('account.config.settings', 'tds_customer_bill')
            i.tds_vendor_payment = ir_values.get_default('account.config.settings', 'tds_vendor_bill')

    @api.multi
    @api.depends('tds_tax_id','amount')
    def cal_tds_amount(self):
        for record in self:
            if record.tds_tax_id and record.amount and record.partner_id:
                amount = record.amount
                if self.invoice_ids:
                    amount = self.invoice_ids[0].amount_untaxed
                dic_taxes = record.tds_tax_id.compute_all(amount, record.currency_id, 1, False, record.partner_id)['taxes']
                tds_amount = 0
                for tax in dic_taxes:
                    tds_amount += tax['amount']
                record.tds_amount = round(tds_amount)
                record.paid_amount = record.amount - record.tds_amount
            else:
                record.tds_amount = 0.0
                record.paid_amount = record.amount - record.tds_amount

    def _create_payment_entry(self, amount):
        """ Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
            Return the journal entry.
        """
        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        invoice_currency = False
        if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
            #if all the invoices selected share the same currency, record the paiement in that currency too
            invoice_currency = self.invoice_ids[0].currency_id
        debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id, invoice_currency)

        move = self.env['account.move'].create(self._get_move_vals())

        #Write line corresponding to invoice payment
        counterpart_aml_dict = self._get_shared_move_line_vals(debit, credit, amount_currency, move.id, False)
        counterpart_aml_dict.update(self.with_context(get_move_id=move.id)._get_counterpart_move_line_vals(self.invoice_ids))
        counterpart_aml_dict.update({'currency_id': currency_id})
        counterpart_aml = aml_obj.create(counterpart_aml_dict)

        #Reconcile with the invoices
        if self.payment_difference_handling == 'reconcile' and self.payment_difference:
            writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
            amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
            # the writeoff debit and credit must be computed from the invoice residual in company currency
            # minus the payment amount in company currency, and not from the payment difference in the payment currency
            # to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
            total_residual_company_signed = sum(invoice.residual_company_signed for invoice in self.invoice_ids)
            total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
            if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
                amount_wo = total_payment_company_signed - total_residual_company_signed
            else:
                amount_wo = total_residual_company_signed - total_payment_company_signed
            # Align the sign of the secondary currency writeoff amount with the sign of the writeoff
            # amount in the company currency
            if amount_wo > 0:
                debit_wo = amount_wo
                credit_wo = 0.0
                amount_currency_wo = abs(amount_currency_wo)
            else:
                debit_wo = 0.0
                credit_wo = -amount_wo
                amount_currency_wo = -abs(amount_currency_wo)
            writeoff_line['name'] = _('Counterpart')
            writeoff_line['account_id'] = self.writeoff_account_id.id
            writeoff_line['debit'] = debit_wo
            writeoff_line['credit'] = credit_wo
            writeoff_line['amount_currency'] = amount_currency_wo
            writeoff_line['currency_id'] = currency_id
            writeoff_line = aml_obj.create(writeoff_line)
            if counterpart_aml['debit']:
                counterpart_aml['debit'] += credit_wo - debit_wo
            if counterpart_aml['credit']:
                counterpart_aml['credit'] += debit_wo - credit_wo
            counterpart_aml['amount_currency'] -= amount_currency_wo
        self.invoice_ids.register_payment(counterpart_aml)

        #Write counterpart lines
        ##### Custom Change Statr######
        amount = self.amount - self.tds_amount
        amount = amount * (self.payment_type in ('outbound', 'transfer') and 1 or -1)
          
        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        invoice_currency = False
        if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
            invoice_currency = self.invoice_ids[0].currency_id
        debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id, invoice_currency)
        ### Custom Change End #####
        if not self.currency_id != self.company_id.currency_id:
            amount_currency = 0
        liquidity_aml_dict = self._get_shared_move_line_vals(credit, debit, -amount_currency, move.id, False)
        liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
        aml_obj.create(liquidity_aml_dict)

        move.post()
        return move
        
    @api.multi
    def create_tds_payment_entry(self,move_id):
        self.ensure_one()
        if self.tds_amount:
            amount = self.tds_amount * (self.payment_type in ('outbound', 'transfer') and -1 or 1)
            aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
            invoice_currency = False
            if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
                invoice_currency = self.invoice_ids[0].currency_id
            debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id, invoice_currency)
            vals= {
                'partner_id': self.payment_type in ('inbound', 'outbound') and self.env['res.partner']._find_accounting_partner(self.partner_id).id or False,
                'move_id': move_id,
                'debit': debit,
                'credit': credit,
                'amount_currency': amount_currency or False,
                'date_maturity':datetime.now()
            }
            vals.update({
            'name': self.name,
            'payment_id': self.id,
            'account_id': self.tds_tax_id.account_id.id,
            'journal_id': self.journal_id.id})

            aml_obj.create(vals)   

    def _get_counterpart_move_line_vals(self,invoice=False):
        res = super(AccountPayment,self)._get_counterpart_move_line_vals(invoice)
        if self.tds_amount:
            if self.env.context and self.env.context.get('get_move_id',False): 
                self.create_tds_payment_entry(self.env.context.get('get_move_id'))
#                 amount = self.amount - self.tds_amount
#                 amount = amount * (self.payment_type in ('outbound', 'transfer') and 1 or -1)
#                   
#                 aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
#                 invoice_currency = False
#                 if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
#                     invoice_currency = self.invoice_ids[0].currency_id
#                 debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id, invoice_currency)
#                 res.update({'debit': debit,
#                 'credit': credit,
#                 'amount_currency': amount_currency or False})
        return res
        